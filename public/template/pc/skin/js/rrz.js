$(document).ready(function() {

    // 手机导航
    $('.menuBtn').click(function(event) {
        $(this).toggleClass('open');
        var _winw = $(window).width();
        var _winh = $(window).height();
        if ($(this).hasClass('open')) {
            if (_winw <= 1199) {
                $('.rrz-nav').stop().slideDown();
            }
        } else {
            if (_winw <= 1199) {
                $('.rrz-nav').stop().slideUp();
            }
        }
    });
    $(window).on('resize', function(e) {
        if ($(window).width() > 1199) {
            $('.menuBtn').removeClass('open');
            $('.hdr').css('display', '');
        }
    });

    // 导航

    if ($(".rrz-nav li").find('dl').length) {
        // $(".nav li").find("dl").siblings("a").attr("href","javascript:;")
    };

    function myNav() {
        var _winw = $(window).width();
        if (_winw >= 1199) {
            $('.rrz-nav').show().addClass('nav-pc').removeClass('nav-m');
            $('body,.menuBtn').removeClass('open');
        } else {
            $('.rrz-nav').hide().addClass('nav-m').removeClass('nav-pc');
        }
    }
    myNav();
    $(window).resize(function(event) {
        myNav();
        $('.menuBtn').removeClass('open');
    });
    $('.nav-pc li').bind('mouseenter', function() {
        $(this).find('dl').stop().slideDown("fast");
        if ($(this).find('dl').length) {
            $(this).addClass('ok');
        }
    });
    $('.nav-pc li').bind('mouseleave', function() {
        $(this).removeClass('ok');
        $(this).find('dl').stop().slideUp("fast");
    });

    $('.nav-m .v1').click(function() {

        $(this).parents('li').siblings('li').find("dl").stop().slideUp("fast");

        // $(this).parents(".nav").find("dl").stop().slideUp("fast");
        if ($(this).siblings('dl').length) {
            $(this).siblings('dl').stop().slideToggle("fast");
            $(this).toggleClass('on');
            return false;
        }
    });

    // 滚动导航悬浮
    $(document).on('scroll', function() {
        var scrollH = $(this).scrollTop();
        if (scrollH > $('.header').height()) {
            $('.rrz-header').addClass('fixed');
        } else {
            $('.rrz-header').removeClass('fixed');
        }
    })

    // 选项卡 鼠标点击
    $(".TAB_CLICK li").click(function() {
        var tab = $(this).parent(".TAB_CLICK");
        var con = tab.attr("id");
        var on = tab.find("li").index(this);
        $(this).addClass('on').siblings(tab.find("li")).removeClass('on');
        $(con).eq(on).show().siblings(con).hide();
    });
    // 自定义单选
    $('[role=radio]').each(function() {
        var input = $(this).find('input[type="radio"]'),
            label = $(this).find('label');

        input.each(function() {
            if ($(this).attr('checked')) {
                $(this).parents('label').addClass('checked');
                $(this).prop("checked", true)
            }
        })

        input.change(function() {
            label.removeClass('checked');
            $(this).parents('label').addClass('checked');
            input.removeAttr('checked');
            $(this).prop("checked", true)
        })
    })


    // 客服
    $('.kf .kf-side').click(function(){
        //$('.kf').animate({ right: '-208' }, "slow");
        var rt = $('.kf').css("right");
        //alert(rt);
        var num = parseInt(rt);
        //alert(num);
        if(num < 0){
            $('.kf').animate({ right: '20px' }, "slow");
            $('.kf .kf-side span.arrow').addClass('on');
        }else{
            $('.kf').animate({ right: '-208px' }, "slow");
            $('.kf .kf-side span.arrow').removeClass('on');
        }
    });
    $('.kt-top span.close').click(function(){
        $('.kf').animate({ right: '-208px' }, "slow");
    });

    //返回顶部
    $('.kf .backTop').click(function() {
        $("html,body").stop().animate({ scrollTop: '0' }, 500);
    });


    $('.rrz-Mobile-nav .mobSo').click(function(event) {
        /* Act on the event */

        $(".top-2 .so").stop().slideToggle(200);
    });

    //内页左侧二级分类
    $('.ul-sub').eq(0).show();
    $('.ul-txt-ins1 > li').click(function(){
    var count = $('.ul-sub', $(this)).size();
    if (count) {
        $(this).find('.ul-sub').toggle();
        $('.ul-sub').not($(this).find('.ul-sub')).hide();
        return false;
    }
    });
    $('.ul-sub li').click(function(event) {
        /* Act on the event */
        event.stopPropagation();
    });
    //首页左侧二级分类
    $('.m-sub01').eq(0).show();
    $('.snv > ul > li').click(function(){
        var count = $('.m-sub01', $(this)).size();
        if (count) {
            $(this).find('.m-sub01').toggle();
            $('.m-sub01').not($(this).find('.m-sub01')).hide();
            return false;
        }
    });

    $('.m-sub01 dd').click(function(event) {
        /* Act on the event */
        event.stopPropagation();
    });

});