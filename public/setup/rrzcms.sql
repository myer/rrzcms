-- ----------------------------------------
-- RRZCMS MySQL Data Transfer 
-- 
-- Server         : 127.0.0.1_3306
-- Server Version : 5.5.53
-- Database       : rrzcms
-- 
-- Part : #1
-- Version : #v1.1.7
-- Date : 2020-10-03 19:58:47
-- -----------------------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Table structure for `rrz_admin`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_admin`;
CREATE TABLE `rrz_admin` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `pen_name` varchar(50) DEFAULT '' COMMENT '笔名（发布文章后显示责任编辑的名字）',
  `true_name` varchar(50) DEFAULT '' COMMENT '真实姓名',
  `mobile` varchar(50) DEFAULT '' COMMENT '手机号码',
  `email` varchar(60) DEFAULT '' COMMENT 'email',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `head_pic` varchar(255) DEFAULT '' COMMENT '头像',
  `last_login` int(11) unsigned DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) DEFAULT '' COMMENT '最后登录ip',
  `login_cnt` int(11) unsigned DEFAULT '0' COMMENT '登录次数',
  `session_id` varchar(50) DEFAULT '' COMMENT 'session_id',
  `parent_id` int(10) unsigned DEFAULT '0' COMMENT '父管理员ID',
  `role_id` int(10) NOT NULL DEFAULT '-1' COMMENT '角色组ID（-1表示超级管理员）',
  `mark_lang` varchar(50) DEFAULT 'cn' COMMENT '当前语言标识',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0=屏蔽，1=正常)',
  `syn_users_id` int(10) unsigned DEFAULT '0' COMMENT '同步注册到会员表',
  `add_time` int(11) unsigned DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) unsigned DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_name` (`user_name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='管理员表';

-- -----------------------------
-- Records of `rrz_admin`
-- -----------------------------
INSERT INTO `rrz_admin` VALUES ('1', 'admin', '', 'admin', '', '', 'e10adc3949ba59abbe56e057f20f883e', '/static/images/dfboy.png', '1601726319', '183.208.105.238', '4', '', '0', '1', 'cn', '1', '1', '1586479354', '1588751715');

-- ----------------------------
-- Table structure for `rrz_admin_log`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_admin_log`;
CREATE TABLE `rrz_admin_log` (
  `id` bigint(16) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `admin_id` int(10) NOT NULL DEFAULT '-1' COMMENT '管理员id',
  `log_info` text COMMENT '日志描述',
  `log_ip` varchar(30) DEFAULT '' COMMENT 'ip地址',
  `log_url` varchar(255) DEFAULT '' COMMENT 'url',
  `log_time` int(11) DEFAULT '0' COMMENT '日志时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `admin_id` (`admin_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='管理员日志表';

-- ----------------------------
-- Table structure for `rrz_admin_role`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_admin_role`;
CREATE TABLE `rrz_admin_role` (
  `role_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) DEFAULT NULL COMMENT '角色名称',
  `act_list` text COMMENT '权限列表',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户角色表';

-- -----------------------------
-- Records of `rrz_admin_role`
-- -----------------------------
INSERT INTO `rrz_admin_role` VALUES ('1', '超级管理员', 'all', '管理全站');

-- ----------------------------
-- Table structure for `rrz_article_nodes`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_article_nodes`;
CREATE TABLE `rrz_article_nodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属模型id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `dir_name` varchar(255) DEFAULT '' COMMENT '目录英文名',
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '父级ID',
  `img` varchar(255) DEFAULT '' COMMENT '栏目图片',
  `id_path` varchar(255) NOT NULL DEFAULT '' COMMENT 'id路径',
  `depth` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '栏目深度',
  `sort` smallint(6) unsigned DEFAULT '50' COMMENT '排序',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '栏目路径',
  `tmpl_path` varchar(100) DEFAULT '' COMMENT '模板',
  `tmpl_view` varchar(100) DEFAULT '' COMMENT '文档模板',
  `seo_title` varchar(255) DEFAULT '' COMMENT 'SEO标题',
  `seo_description` mediumtext COMMENT '分类描述',
  `seo_keywords` varchar(200) DEFAULT '' COMMENT '搜索关键词',
  `content` longtext COMMENT '文章内容',
  `wap_content` longtext COMMENT '手机端内容',
  `ifpub` enum('true','false') NOT NULL DEFAULT 'true' COMMENT '发布',
  `uptime` int(10) unsigned DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_parent_id` (`parent_id`) USING BTREE,
  KEY `idx_ifpub` (`ifpub`) USING BTREE,
  KEY `idx_channel_id` (`channel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='文章类目';

-- -----------------------------
-- Records of `rrz_article_nodes`
-- -----------------------------
INSERT INTO `rrz_article_nodes` VALUES ('1', '0', '关于我们', '', '0', '', '1', '1', '1', '1001', '', '', '', '', '', '', '', 'true', '1589007347');
INSERT INTO `rrz_article_nodes` VALUES ('2', '0', '新闻动态', '', '0', '', '2', '1', '2', '1002', '', '', '', '', '', '', '', 'true', '1589007378');
INSERT INTO `rrz_article_nodes` VALUES ('3', '0', '成功案例', '', '0', '', '3', '1', '3', '1003', 'img', '', '', '', '', '', '', 'true', '1589007388');
INSERT INTO `rrz_article_nodes` VALUES ('4', '0', '公司动态', '', '2', '', '2,4', '2', '1', '10021001', '', '', '', '', '', '', '', 'true', '1589007444');
INSERT INTO `rrz_article_nodes` VALUES ('5', '0', '行业资讯', '', '2', '', '2,5', '2', '2', '10021002', '', '', '', '', '', '', '', 'true', '1589007455');

-- ----------------------------
-- Table structure for `rrz_articles`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_articles`;
CREATE TABLE `rrz_articles` (
  `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `node_id` mediumint(8) NOT NULL DEFAULT '0' COMMENT '类别ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '文章标题',
  `img` varchar(255) NOT NULL DEFAULT '' COMMENT '文章配图',
  `tmpl_path` varchar(100) DEFAULT '' COMMENT '模板',
  `content` longtext COMMENT '文章内容',
  `wap_content` longtext COMMENT '手机端内容',
  `author` varchar(100) NOT NULL DEFAULT '' COMMENT '文章作者',
  `author_email` varchar(60) NOT NULL DEFAULT '' COMMENT '作者邮箱',
  `uptime` int(10) unsigned DEFAULT '0' COMMENT '更新时间',
  `pubtime` int(10) unsigned DEFAULT '0' COMMENT '文章发布时间',
  `ifpub` enum('true','false') NOT NULL DEFAULT 'false' COMMENT '发布',
  `seo_title` varchar(255) DEFAULT '' COMMENT 'SEO标题',
  `seo_description` mediumtext COMMENT '分类描述',
  `seo_keywords` varchar(200) DEFAULT '' COMMENT '搜索关键词',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '文章创建时间',
  `is_head` tinyint(1) unsigned DEFAULT '0' COMMENT '头条（0=否，1=是）',
  `is_special` tinyint(1) unsigned DEFAULT '0' COMMENT '特荐（0=否，1=是）',
  `is_recom` tinyint(1) unsigned DEFAULT '0' COMMENT '推荐（0=否，1=是）',
  `sort` smallint(6) unsigned DEFAULT '100' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_node_id` (`node_id`) USING BTREE,
  KEY `idx_is_head` (`is_head`) USING BTREE,
  KEY `idx_is_special` (`is_special`) USING BTREE,
  KEY `idx_is_recom` (`is_recom`) USING BTREE,
  KEY `idx_ifpub` (`ifpub`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='文章';

-- -----------------------------
-- Records of `rrz_articles`
-- -----------------------------
INSERT INTO `rrz_articles` VALUES ('1', '1', '公司简介', '', 'single', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">广东某某绿化有限公司成立于2005年2月。自创立以来，经过十多年的开拓和发展，现已成为一家集园林景观设计、景观环境投资建设、苗木生产研发、生态环境治理等四大核心业务板块的大型综合性园林企业。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">公司注册资金12800万元，总资产逾4亿元，同时具备国家园林绿化一级、市政工程施工总承包二级、古建筑工程专业承包三级.并且拥有环境生态修复工程设计甲级、水污染治理工程设计甲级、固废处理处置工程设计甲级等浙江省环境污染防治专项工程设计认可证书和环境生态修复工程总承包甲级、水污染治理工程总承包甲级、固废处理处置工程总承包甲级等浙江省环境污染治理工程总承包资质证书。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">公司拥有各类专业技术人员450余人，其中工程技术人员150余人，中、高级职称专业技术人员50余人。公司长期聘请园林行业权威专家为技术顾问，培养了一支富有实践经验的高素质施工管理人员和施工队伍，为公司在园林设计、工程建设、苗木培育、环境治理等方面的业务拓展和实施提供了强有力的保障。&nbsp;</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">公司坚持秉承“为客户提供满意作品至上”的企业宗旨和“诚信、敬业、高效、创新”的工作理念。经过不断地开拓和发展，先后获得了“广州市农业龙头企业”、“重合同守信用企业”、“安全生产暨优秀单位”、“重质量、守诚信、讲信誉百家优秀建筑企业”等荣誉称号，通过了ISO9001质量管理、ISO14001环境管理和OHSAS18001职业健康安全管理三体系认证，连续多年被政府和银行评为浙江省“AAA级守合同、重信用”单位、“AAA级资信企业”、“企业管理信誉AAA级”和“广东省重点办信用AAA级”，是中国风景园林协会、广东省风景园林学会和广州市风景园林学会会员单位，广东省环境治理团队会员单位。&nbsp;</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">公司自创建以来，以南部区域为核心市场，不断拓展业务范围，近年来，承接了包括BT项目在内的大批园林绿化景观建设项目，多次获得广东省“优秀园林工程”金、银、铜奖和广州市“优秀园林工程”金奖、广州市“优秀绿化工程”等众多工程成果，社会效益和经济效益逐年稳步提高。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">回首过往，展望未来，公司将进一步完善公司运营机制，强化人员管理和人才队伍培育，力求通过精湛的设计、精细的施工、精心的服务，不断为客户提供满意的作品，为构建文明、和谐、优雅、生态的人居社会环境，做出应有的贡献。</p><p><br/></p>', '    ', '', '', '1589011634', '1589007501', 'true', '', '广东某某绿化有限公司成立于2005年2月。自创立以来，经过十多年的开拓和发展，现已成为一家集园林景观设计、景观环境投资建设、苗木生产研发、生态环境治理等四大核心业务板块的大型综合性园林企业。公司注册资金12800万元，总资产逾4亿元，同时具备国家园林', '', '44', '1589007501', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('2', '1', '企业文化', '', 'single', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">随着市场竞争日趋激烈，企业文化已成为企业核心竞争力量不可缺少的重要元素，天宏园林企业文化表现“人性”、“勤奋”、”责任“、“诚信”</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\"><br style=\"text-indent: 2em;\"/>企业精神：海纳百川，有德乃大，上善若水，诚信天下<br style=\"text-indent: 2em;\"/>企业文化：沟通、理解、合作、成功<br style=\"text-indent: 2em;\"/>企业宗旨：求实、创新、拼搏<br style=\"text-indent: 2em;\"/>企业承诺：以人为本，诚信客户<br style=\"text-indent: 2em;\"/>企业方针：严格规范管理，不断提高改进，做园林绿化行业领头人<br style=\"text-indent: 2em;\"/>企业目标：为现代城市公共空间增添绿色活力和魅力</p><p><br/></p>', '    ', '', '', '1589011630', '1589007526', 'true', '', '随着市场竞争日趋激烈，企业文化已成为企业核心竞争力量不可缺少的重要元素，天宏园林企业文化表现“人性”、“勤奋”、”责任“、“诚信”企业精神：海纳百川，有德乃大，上善若水，诚信天下企业文化：沟通、理解、合作、成功企业宗旨：求实、创新、拼搏企业承诺：以人', '', '13', '1589007526', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('3', '4', '园林绿化中的草制品起到了什么作用？', '/storage/images/20200509/5d27eaca97797.png', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">草制品制作简单，用途广泛，并且在园林绿化中也有着非常重要的作用，是园林中常用到的维护品之一。下面，北京园林绿化公司就给大家介绍一下草制品在园林绿化中的作用。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">通常草制品绑扎时间约为3—4年，即树木度过泛苗期的时刻。路段新栽植树苗草制品未取的因素就在于此。对于新栽法桐“逝世”较多的首要因素是：法桐在高度、胸径、弯度、根径等标准上请求高，只要在多个当地安排树苗，因为各地土质、气候、湿度不一样，泛苗期犬牙交错，如今感官上判别现已逝世的树木，大多数还处于泛苗期。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27eaca97797.png\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">包树木草制品的首要作用为在夏日可抵挡酷日暴晒，冬季可防治冻伤树干，下雨了可盖水保墒修养水分。因此，在城市绿化办理维护过程中，草制品一概不取，旧的草制品脱落后，为提高树木成活率，园林部门还会从头绑扎。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">树干绑绳子的树木通常分为两种:一种是新移栽的树,另外一种是宝贵树种,也有喜爱植物的爱好者以及园林绿化工给要越冬的植物绑上草制品.树干绑草制品的因素有：新移栽的树为了防止树皮在运送中擦破影响成活率。防止阳光直射在树干上,减少水分丢失减小蒸腾作用。越冬的树木绑上草制品能够降低树干因为冷空气的因素而冻伤的可能性。</p><p><br/></p>', '    ', '', '', '1589011625', '1589007705', 'true', '', '草制品制作简单，用途广泛，并且在园林绿化中也有着非常重要的作用，是园林中常用到的维护品之一。下面，北京园林绿化公司就给大家介绍一下草制品在园林绿化中的作用。通常草制品绑扎时间约为3—4年，即树木度过泛苗期的时刻。路段新栽植树苗草制品未取的因素就在于此', '', '18', '1589007705', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('4', '4', '园林绿化工程类别是怎么划分的？', '/storage/images/20200509/5d27eb9d0bc19.png', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">园林绿化工程类别划分为一类工程、二类工程、三类工程和四类工程。那么，绿化工程的划分标准是什么呢？园林绿化公司带大家了解。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">绿化工程类别划分标准：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">一类工程：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、单项建筑面积600平方米及以上的园林建筑工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、高度21米及以上的仿古塔。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、高度9米及以上的牌楼、牌坊。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、25000平方米及以上综合性园林建设。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、缩景模仿工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">6、堆砌英石山50吨及以上或景石(黄腊石、太湖石、花岗石)150吨及以上或塑9米高及以上的假石山。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">7、单条分车绿化带宽度5米、道路种植面积15000平方米及以上的绿化工程；</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">8、两条分车绿化带累计宽度4米、道路种植面积12000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">9、三条及以上分车绿化带(含路肩绿化带)累计宽度20米、 道路种植面积60000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">10、公园绿化面积30000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">11、宾馆、酒店庭园绿化面积1000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">12、天台花园绿化面积500平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">13、其它绿化累计面积20000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\"><img src=\"/storage/images/20200509/5d27eb9d0bc19.png\" style=\"width: 63%; height: auto;\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">二类工程：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、单项建筑面积300平方米及以上的园林建筑工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、高度15米及以上的仿古塔。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、高度9米以下的重檐牌楼、牌坊。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、20000平方米及以上综合性园林建设。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、景区园桥和园林小品。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">6、园林艺术性围墙(带琉璃瓦顶、琉璃花窗或景门窗)。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">7、堆砌英石山20吨及以上或景石(黄腊石、太湖石、花岗石)80吨及以上或塑6米高及以上的假山石。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">8、单条分车绿化带宽度5米、道路种植面积10000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">9、两条分车绿化带累计宽度4米、道路种植面积8000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">10、三条及以上分车绿化带(含路肩绿化带)累计宽度15米、道路种植面积40000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">11、公园绿化面积20000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">12、宾馆、酒店庭园绿化面积800平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">13、天台花园绿化面积300平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">14、其它绿化累计面积15000平方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\"><img src=\"/storage/images/20200509/5d27eb9d670b3.png\" style=\"width: 65%; height: auto;\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">三类工程：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、单项建筑面积300平方米以下的园林建筑工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、高度15米以下的仿古塔。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、高度9米以下的单檐牌楼、牌坊。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、10000平方米及以上综合性园林建设。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、堆砌英石山20吨以下或景石(黄腊石、太湖石、花岗石) 80吨以下或塑6米高以下的假石山。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">6、庭院园桥和园林小品。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">7、园路工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">8、单条分车绿化带宽度5米、道路种植面积10000平方米以下的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">9、两条分车绿化带累计宽度4米、道路种植面积8000平方米以下的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">10、三条及以上分车绿化带(含路肩绿化带)累计宽度15米、道路种植面积40000平方米以下的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">11、公园绿化面积20000平方米以下的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">12、宾馆、酒店庭园绿化面积800平方米以下的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">13、天台花园绿化面积300平方米以下的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">14、其它绿化累计面积10000方米及以上的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\"><img src=\"/storage/images/20200509/5d27eb9dd5021.png\" style=\"width: 67%; height: auto;\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">四类工程：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、10000平方米以下综合性园林建设。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、园林一般围墙、围栏。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、砌筑花槽、花池。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、仅有路肩绿化的绿化工程</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、道路断面仅有人行道路树木的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">6、其他绿化累计面积10000平方米以下的绿化工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、园林建筑工程指亭、廊、舫、榭斋、馆、轩、塔等建筑工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、园林小品是指门楼、景墙、景壁造假山、树(5米以下)、竹、凳小型工艺建筑。厅堂、楼、阁、殿、</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、综合性园林建设是指景观、公园、游乐场、公园式墓园等地域的一切园林建设。它是按园林建设规模划分类别其建设面积以工程立项批文为准。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、缩景模仿工程是指特定的建筑物、景区和石景、自然树、动植物等景观的缩小、模仿、塑造工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、景区园林小品是指风景、名胜、公园、墓园、宾馆、别墅区等地域的园林小品。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">6、庭院园林小品是指厂矿、机关、学校、住宅小区等地域的园林小品。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">7、绿化工程是指市政绿化种植和迁移树木工程，以及住宅区、工厂、宾馆、酒店、别墅等庭院绿化布置工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">8、道路绿化工程是指按定额规定工作内容，包括道路两旁乔灌木、绿化带地被等在内的工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">9、庭院绿化工程是指按定额规定的工作内容，包括花坛、 草皮、绿篱、乔灌木等在内的工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">10、在单位工程中，有几个特征时，凡符合其中一个特征者，即为该类工程。</p><p><br/></p>', '    ', '', '', '1589011621', '1589007903', 'true', '', '园林绿化工程类别划分为一类工程、二类工程、三类工程和四类工程。那么，绿化工程的划分标准是什么呢？园林绿化公司带大家了解。绿化工程类别划分标准：一类工程：1、单项建筑面积600平方米及以上的园林建筑工程。2、高度21米及以上的仿古塔。3、高度9米及以上', '', '12', '1589007903', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('5', '4', '园林绿化公司带您了解夏季绿化养护小知识', '/storage/images/20200509/5d27ecdf0a737.png', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">夏季到来，气温逐渐升高 ，7月雨季将至，高温高湿，绿化苗木进入旺盛的生长期，病虫害高发，要及时做好绿化养护，切实维护绿化成果。今天，园林绿化公司小编给大家讲一讲夏季绿化养护的几个要点。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、浇水：树木、灌木、草坪、绿篱及各种色块进入生长旺盛期，植物需水量很大，根据雨量适时调节浇水量，满足苗木生长需求，并及时松土保墒。新栽的树，浇水时间应尽量选在上午9点以前，下午5点以后，因为在烈日当空时浇水会因为冷热刺激导致植物根系受伤。如果只需要浇水一次，最好是选择在上午，或者夜间。浇水要浇透，尽量采用大水浇灌，保证植物正常生长的同时要考虑蒸发量。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、施肥：根据植物的生长发育情况，结合浇水增施肥料（特别是化肥）。加强整体的观赏效果，遵循“弱多强少”，做到有针对性的局部补施肥，施肥量要小，特别是速效氮肥。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27ecdf0a737.png\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、修剪：雨季将至，可将树冠大、叶密、根浅的苗木（洋槐、法国梧桐等）适当进行疏剪，对与高压电线及路灯、建筑物有矛盾的枝杈也应进行剪除，同时要及时去除根蘖及疯蘖。剪除因刮风下雨而折断等而枯黄的枝叶，维持树型均衡树势。剪除残花，减少营养损失。绿篱及色块要剪除个别长枝条进行局部修整，保证造型轮廓明显，层次分明,提高观赏效果。草坪修剪，基本上10－15天一次，高度保持在5－9公分左右，也可以适当放低修剪，增加通透性，减少病害的发生。修剪时注意不要有明显的漏剪痕迹，剪下的草屑及时清运，不得堆积在草坪里。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、病虫害的防治：主要需要防治的是槐树、无患子等树上的各类蚜虫，如紫薇长斑蚜、粘虫等，可用吡虫啉防治；对黄山栾树等树上的蛀干类害虫可用杀螟松除治。此外，杨柳、合欢等树上的光肩天牛，槐树、无患子等树上的国槐尺蠖、黄刺蛾等各种蛾类害虫，碧桃、月季等树木上的月季黑斑病、白蜡黑斑、柏锈病等在夏季也是高发期，需加强防治。还要注意介壳虫，及草坪病害。全面、均匀地喷洒药物，药物种类要交替使用，避免产生抗药性。喷洒时要注意风向，保证安全。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27ecdf65db8.png\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、除草：及时清除草坪及各种树木下的杂草，绿篱及色块内生出的杂生植物、爬藤等应及时予以连根清除，防止草荒。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">6、准备排水：雨季将至，对于低洼易积水的绿化区域，应预先挖好排水沟等，以便解急。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">7、移植常绿树：雨季期间，水分充足，空气湿度大，蒸发量低，可以移植常绿树。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">8、维护：雨季多风雨，容易发生树木歪倒等情况，应事先做好物资、人力、设备等方面的准备。随时检查，发现情况及时处理，及时扶正歪倒的苗木或支立柱。</p><p><br/></p>', '    ', '', '', '1589011616', '1589008014', 'true', '', '夏季到来，气温逐渐升高 ，7月雨季将至，高温高湿，绿化苗木进入旺盛的生长期，病虫害高发，要及时做好绿化养护，切实维护绿化成果。今天，园林绿化公司小编给大家讲一讲夏季绿化养护的几个要点。1、浇水：树木、灌木、草坪、绿篱及各种色块进入生长旺盛期，植物需水', '', '12', '1589008014', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('6', '4', '园林绿化公司为您解析树木长势衰弱、叶小发黄的原因', '/storage/images/20200509/5d27ecfcec4f0.png', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">园林绿化公司发现在一些苗圃里有一些树木长势弱、叶小发黄，但既没发现病害，也没发现虫害，那么，很可能是因为土壤板结。下面，我们一起来了解土壤板结的原因。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">土壤的团粒结构是土壤肥力的重要指标，土壤团粒结构的破坏致使土壤保水、保肥能力及通透性降低，造成土壤板结。有机质的含量是土壤肥力和团粒结构的一个重要指标，有机质的降低，致使土壤板结。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27ecfcec4f0.png\" style=\"width: 65%; height: auto;\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1. 长期单一施用化学肥料</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">腐殖质不能得到及时地补充，不但会引起土壤板结,还可能龟裂。化肥造成土壤板结的原因是化肥中只有阳离子或阴离子是植物所需要的元素，植物单方面选择吸收了有用的离子，造成土壤酸化或盐碱化。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2. 过量施入氮肥</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">土壤有机质是土壤团粒结构的重要组成部分，土壤有机质的分解是以微生物的活动来实现的。向土壤中过量施入氮肥后，微生物的氮素供应增加1份，相应消耗的碳素就增加25份，所消耗的碳素来源于土壤有机质，有机质含量低，影响微生物的活性，从而影响土壤团粒结构的形成，导致土壤板结。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27ecfd627d5.png\" style=\"width: 65%; height: auto;\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3. 过量施入磷肥</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">土壤中的阳离子以2价的钙、镁离子为主，向土壤中过量施入磷肥时，磷酸根离子与土壤中钙、镁等阳离子结合形成难溶性磷酸盐，即浪费磷肥，又破坏了土壤团粒结构，致使土壤板结。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4. 过量施入钾肥</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">向土壤中过量施入钾肥时，钾肥中的钾离子置换性特别强，能将形成土壤团粒结构的多价阳离子置换出来，而一价的钾离子不具有键桥作用，土壤团粒结构的键桥被破坏了，也就破坏了团粒结构，致使土壤板结。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">土壤板结的情况下，缺氧而导致根系活力下降，不能正常发育，植物根部细胞呼吸减弱，氮素等营养又多以离子态存在，吸收时要消耗细胞代谢产生的能量，呼吸减弱，故能量供应不足，影响养分的吸收。</p><p><br/></p>', '    ', '', '', '1589011608', '1589008104', 'true', '', '园林绿化公司发现在一些苗圃里有一些树木长势弱、叶小发黄，但既没发现病害，也没发现虫害，那么，很可能是因为土壤板结。下面，我们一起来了解土壤板结的原因。土壤的团粒结构是土壤肥力的重要指标，土壤团粒结构的破坏致使土壤保水、保肥能力及通透性降低，造成土壤板', '', '16', '1589008104', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('7', '4', '园林绿化不同等级资质承揽工程范围', '/storage/images/20200509/5d27ed17a9c53.png', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">园林绿化资质分为一级资质、二级资质以及三级资质等等。园林绿化企业不同等级的资质公司所能承揽的工程的范围是不一样的。关于每个等级资质所能承揽的工程范围，将由园林绿化公司为大家讲述。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">园林绿化不同等级资质承揽工程范围：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">一级园林资质经营范围：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、 可承揽各种规模以及类型的园林绿化工程，包括：综合公园、社区公园、专类公园、带状公园等各类公园，生产绿地、防护绿地、附属绿地等各类绿地。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、可承揽园林绿化工程中的整地、栽植及园林绿化项目配套的500平方米以下的单层建筑（工具间、茶室、卫生设施等）、小品、花坛、园路、水系、喷泉、假山、雕塑、广场铺装、驳岸、单跨15米以下的园林景观人行桥梁、码头以及园林设施、设备安装项目等。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、 可承揽各种规模以及类型的园林绿化养护管理工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、 可从事园林绿化苗木、花卉、盆景、草坪的培育、生产和经营。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、 可从事园林绿化技术咨询、培训和信息服务。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27ed17a9c53.png\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">二级园林资质经营范围：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、 可承揽工程造价在1200万元以下的园林绿化工程，包括：综合公园、社区公园、专类公园、带状公园等各类公园，生产绿地、防护绿地、附属绿地等各类绿地。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、可承揽园林绿化工程中的整地、栽植及园林绿化项目配套的200平方米以下的单层建筑（工具间、茶室、卫生设施等）、小品、花坛、园路、水系、喷泉、假山、雕塑、广场铺装、驳岸、单跨10米以下的园林景观人行桥梁、码头以及园林设施、设备安装项目等。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、 可承揽各种规模以及类型的园林绿化养护管理工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、 可从事园林绿化苗木、花卉、盆景、草坪的培育、生产和经营，园林绿化技术咨询和信息服务。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27ed181466b.png\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">三级园林资质经营范围：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、 可承揽工程造价在500万元以下园林绿化工程，包括：综合公园、社区公园、专类公园、带状公园等各类公园，生产绿地、防护绿地、附属绿地等各类绿地。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、 可承揽园林绿化工程中的整地、栽植及小品、花坛、园路、水系、喷泉、假山、雕塑、广场铺装、驳岸、单跨10米以下的园林景观人行桥梁、码头以及园林设施、设备安装项目等。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、 可承揽各种规模以及类型的园林绿化养护管理工程。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、 可从事园林绿化苗木、花卉、草坪的培育、生产和经营。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">四级园林资质经营范围：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">可承担工程造价在50万元以下的纯绿化工程项目、园林绿化养护工程以及劳务分包。</p><p><br/></p>', '    ', '', '', '1589011603', '1589008180', 'true', '', '园林绿化资质分为一级资质、二级资质以及三级资质等等。园林绿化企业不同等级的资质公司所能承揽的工程的范围是不一样的。关于每个等级资质所能承揽的工程范围，将由园林绿化公司为大家讲述。园林绿化不同等级资质承揽工程范围：一级园林资质经营范围：1、 可承揽各种', '', '14', '1589008180', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('8', '4', '园林绿化改造工程施工常见误区，您中招了吗？', '/storage/images/20200509/5d27ed45293f0.png', '', '<h3 style=\"color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">园林绿化改造工程施工常见误区:</h3><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1、雨天栽植</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">绿化施工人员为抢时，抓进度，顶着大雨栽植苗木，这种行为似乎可以减少浇水这一环节，实则这种行为不可取，这样栽植根部被糊状泥土埋压，通透性极差，不利于苗木的成活生长。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2、带袋栽植</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">绿化施工人员为了省工常将苗木连同营养袋(或者遮阴网、草绳等）一同埋入土中，这种省工的行为虽然能保证苗木栽后短时间内的成活，但由于营养袋较难腐烂，限制了苗木根系四周生长，从而易形成“老小株”苗；同样草绳经长时间腐烂后对土壤的理化性状会造成一定的破坏。建议对袋苗必须先除去营养袋后再栽植为宜。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5d27ed45293f0.png\" style=\"width: 67%; height: auto;\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3、垃圾土上栽植</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">绿化施工人员开穴栽苗时，对穴内的垃圾诸如塑料袋、石灰渣、砖块等不予清除，将苗木直接栽植，苗木在这种恶劣的小环境中成活率无疑极低。建议在开挖穴时遇到建筑垃圾或生活垃圾等杂物时，一定要清理彻底然后再栽植，确保建植绿地的质量。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">4、栽植过深或过浅</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">绿化施工人员易忽视苗木土球的大小和苗木根系的深浅而将苗木放入穴中覆土而成，这样对小苗和浅根系苗木易造成栽植过深埋没了根颈部，苗木生长极度困难，甚至因根部积水过多而窒息；对大苗木和深根性苗木易造成栽植过浅，根部易受冻害和日灼伤，风吹易倒伏等。绿化养护公司建议要视苗木的大小和苗木根系的深浅来确定栽植深度，以苗木根颈部露于表土层为宜。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">5、未能及时浇透水</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">绿地栽植过程中遇到降雨或乌云压境时，施工人员常忽视了栽后浇水的环节，这极易因雨水里不够导致苗木缺水而死亡。建议待雨停后立即补浇透浇足。</p><p><br/></p>', '    ', '', '', '1589011598', '1589008245', 'true', '', '园林绿化改造工程施工常见误区:1、雨天栽植绿化施工人员为抢时，抓进度，顶着大雨栽植苗木，这种行为似乎可以减少浇水这一环节，实则这种行为不可取，这样栽植根部被糊状泥土埋压，通透性极差，不利于苗木的成活生长。2、带袋栽植绿化施工人员为了省工常将苗木连同营', '', '17', '1589008245', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('9', '4', '绿化苗木的培育栽植方法介绍', '', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">通过实践证明，在对绿化苗木进行栽植时，工作人员首先需要明确其与经济型果园的建立在性质上存在明显不同，也就是说苗木的栽植需要在单位面积内尽量对苗木质量与数量进行提升。绿化公司总结整理了以下几种栽植方法，这些方法与我国大部分地区的环境和需求相适应，一起来了解一下都是哪些方法吧。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1.确定栽植方法</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">①采取大垄、大株距的方式对苗木进行栽植；</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">②栽植后覆土、踏实，保证中间低、两边高，这样做的目的是为灌水工作提供便利；</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">③待灌水工作完成后，再在两边覆土，避免水分的不必要失散。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2.确定管理方法</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">当所栽植苗木的抽生长度达到10厘米时，便可以开始第一次的人工抹芽，这次人工抹芽需要将除位于中上部的5～6个抽生枝条之外的枝条全部抹掉，同时还需要开展相应的根外追肥和喷药工作，在对叶面进行喷雾处理的过程中，工作人员应当以杀虫剂和0.3% 尿素液为主，这样做能够在一定程度上对枝条的生长速度进行加快，还能够对病虫害的发生起到抑制作用。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><br/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">在7、8月时，所栽植苗木的抽生长度达到60～70厘米，此时便可以开始第二次的人工整形，二次整形的目的是对树型进行培养，因此，在绿化的同时工作人员需要做好相应的树型，对位于不同方位的枝条进行搭配，在避免重叠或交叉的基础上，选择3～5个相对健壮的枝条作为主枝培养，其余枝条则应当从基部剪掉，这样做的目的是避免养分被过多消耗，保证枝条的快速成熟。实践结果表明，应用上文所叙述的管理方法，当年秋季就能够完成对二年生优质苗木进行培养的工作。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">二、绿化苗木的栽植</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">1.测距、挖坑</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">根据实际需求完成苗木的测距工作。所种植绿化苗木的单行株距应当被控制在2～4米的范围内，每两行之间的行距则是3～4米。在对需要挖掘的坑的大小进行确认时，工作人员应当将苗木的年龄作为主要判断依据，树龄约3/4年的苗木，所对应的坑应为直径50厘米、深度30厘米，并且在坑中应用表土与农家肥进行混拌形成的肥料进行填土处理，如果没有足够的农家肥供工作人员使用，则可以选择磷酸二铵和尿素对其进行代替。对于需要挖掘处理的苗木而言，工作人员在对苗木根部直径和深度进行确定时，应当将坑的大小和树龄情况作为主要参考依据，切忌出现随意挖坑的情况。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">2.挖掘、运输</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">在当地进行栽植的苗木可以做到随挖随栽。对异地栽植的苗木而言，工作人员首先需要完成对定植坑的挖掘工作，在对大龄苗木进行挖掘的过程中，应当对以下两个时期引起足够的重视：</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">①初冬季节（11月中旬至12月初），此时应当带大冻土坨，保证苗木的根部不会受伤；</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">②早春季节（3月中旬至4月初），此时也需要带大冻土坨，并且在主干上做好相应的方向标记。在苗木运输的过程中，可以在根部缠绕粗草绳，保持树体的水分直至到达目的地。</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\"><br/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">3.定植</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">根据相应的标记，将带大冻土坨的苗木放入相对应的定植坑之中，并且用土在四周进行封严。在完成栽植工作后，应当立即开始对苗木进行灌水，初冬季节的灌水共分为2次，每次灌水之间相隔10天，保证地上部封冻；早春季节则应当在灌透底水的同时，将地膜覆盖在树盘的周围，这样做能够保湿增温，加快苗木根系的生长速度。</p><p><br/></p>', '    ', '', '', '1589011593', '1589008365', 'true', '', '通过实践证明，在对绿化苗木进行栽植时，工作人员首先需要明确其与经济型果园的建立在性质上存在明显不同，也就是说苗木的栽植需要在单位面积内尽量对苗木质量与数量进行提升。绿化公司总结整理了以下几种栽植方法，这些方法与我国大部分地区的环境和需求相适应，一起来', '', '17', '1589008365', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('10', '3', '案例展示一', '/storage/images/20200509/0e17196caf8ac034bed73c7e1187f903.jpg', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/0e17196caf8ac034bed73c7e1187f903.jpg\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</p><p><br/></p>', '    ', '', '', '1589011589', '1589008475', 'true', '', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '', '10', '1589008475', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('11', '3', '案例展示二', '/storage/images/20200509/b984ecc6d23309a6e622b0a310c994d0.jpg', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"http://127.0.0.1:9192/uploads/ueditor/20190712/b984ecc6d23309a6e622b0a310c994d0.jpg\" title=\"案例展示二(图1)\" alt=\"案例展示二(图1)\" style=\"height: auto;\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</p><p><br/></p>', '      ', '', '', '1589011584', '1589008533', 'true', '', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '', '12', '1589008533', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('12', '3', '案例展示三', '/storage/images/20200509/5853c6de14f1317d3556327ac73f6b00.jpg', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/5853c6de14f1317d3556327ac73f6b00.jpg\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</p><p><br/></p>', '    ', '', '', '1589011580', '1589008575', 'true', '', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '', '12', '1589008575', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('13', '3', '案例展示四', '/storage/images/20200509/30237432651334f7b74f907ed24493c3.jpg', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/30237432651334f7b74f907ed24493c3.jpg\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</p><p><br/></p>', '    ', '', '', '1589011576', '1589008621', 'true', '', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '', '11', '1589008621', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('14', '3', '案例展示五', '/storage/images/20200509/ad6e6a1c04dbfe52234edb10bc6bde02.jpg', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/ad6e6a1c04dbfe52234edb10bc6bde02.jpg\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</p><p><br/></p>', '    ', '', '', '1589011571', '1589008723', 'true', '', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '', '10', '1589008723', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('15', '3', '案例展示六', '/storage/images/20200509/31eccaf1004f308145d7a3f08c7c0ed4.jpg', '', '<p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><img src=\"/storage/images/20200509/31eccaf1004f308145d7a3f08c7c0ed4.jpg\"/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</p><p><br/></p>', '    ', '', '', '1589011556', '1589008761', 'true', '', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '', '10', '1589008761', '0', '0', '0', '100');
INSERT INTO `rrz_articles` VALUES ('16', '0', '联系我们', '', 'single', '<h3 style=\"color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\">广州某某园林绿化公司</h3><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\">电话: 400-123-4567<br/>传真: + 86-123-4567<br/>QQ:1234567890<br/>邮箱: admin@youweb.com<br/>地址: 广东省广州市天河区某某工业区88号</p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal;\"><br/></p><p style=\"margin-top: 5px; margin-bottom: 5px; color: rgb(0, 0, 0); font-family: sans-serif; white-space: normal; text-align: center;\"><iframe class=\"ueditor_baidumap\" src=\"/plugins/wxeditor/ueditor/dialogs/map/show.html#center=116.404,39.915&zoom=10&width=530&height=340&markers=116.404,39.915&markerStyles=l,A\" frameborder=\"0\" width=\"534\" height=\"344\"></iframe></p><p><br/></p>', '          ', '', '', '1589017153', '1589009650', 'true', '', '广州某某园林绿化公司电话: 400-123-4567传真: + 86-123-4567QQ:1234567890邮箱: admin@youweb.com地址: 广东省广州市天河区某某工业区88号', '', '24', '1589009650', '0', '0', '0', '100');

-- ----------------------------
-- Table structure for `rrz_channelfield`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_channelfield`;
CREATE TABLE `rrz_channelfield` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '字段名称',
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属模型id',
  `channel_type` enum('articles','goods') NOT NULL DEFAULT 'articles' COMMENT '类型（articles：文章，goods：产品）',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '字段标题',
  `dtype` varchar(32) NOT NULL DEFAULT '' COMMENT '字段类型',
  `define` text NOT NULL COMMENT '字段定义',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大长度，文本数据必须填写，大于255为text类型',
  `dfvalue` longtext COMMENT '默认值',
  `dfvalue_unit` varchar(50) NOT NULL DEFAULT '' COMMENT '数值单位',
  `remark` varchar(256) NOT NULL DEFAULT '' COMMENT '提示说明',
  `is_filter` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否应用于条件筛选',
  `ifrequire` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `sort` int(5) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_channel_id` (`channel_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='自定义字段表';


-- ----------------------------
-- Table structure for `rrz_channeltype`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_channeltype`;
CREATE TABLE `rrz_channeltype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '表ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `type` enum('articles','goods') NOT NULL DEFAULT 'articles' COMMENT '类型（articles：文章，goods：产品）',
  `add_time` int(11) unsigned DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) unsigned DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='文章（产品）类型';


-- ----------------------------
-- Table structure for `rrz_config`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_config`;
CREATE TABLE `rrz_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '' COMMENT '配置的key键名',
  `value` text COMMENT '配置内容',
  `type` varchar(64) DEFAULT '' COMMENT '类型',
  `desc` varchar(50) DEFAULT '' COMMENT '描述',
  `lang` varchar(50) DEFAULT 'cn' COMMENT '语言标识',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已删除（0：否，1：是）',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_name` (`name`) USING BTREE,
  KEY `idx_type` (`type`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统配置表';

-- -----------------------------
-- Records of `rrz_config`
-- -----------------------------
INSERT INTO `rrz_config` VALUES ('1', 'status', '1', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('2', 'name', '某某网络科技有限公司', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('3', 'logo', '/storage/images/20200509/4925d98344c09d70aab21a475271707a.png', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('4', 'wap_logo', '', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('5', 'favicon', '/storage/images/20200509/9e9ffe4e859c185d063363237637f296.png', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('6', 'copyright', 'Copyright © 2012-2018 某某公司 版权所有', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('7', 'recordnum', '琼ICP备xxxxxxxx号', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('8', 'title', '响应式苗木园林绿化公司模板', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('9', 'keywords', '', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('10', 'description', '', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('11', 'is_authorization', '1', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('12', 'authortoken_code', '', 'website', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('13', 'popup_upgrade', '1', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('14', 'wap_domain', '', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('15', 'is_https', '0', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('16', 'logo', '/static/images/logo.png', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('17', 'sqldatapath', '', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('18', 'expiretime', '3600', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('19', 'name', '广州某某苗木公司', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('20', 'telephone', '400-123-4567', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('21', 'fax', '020-99999999', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('22', 'qq', '88888888', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('23', 'email', 'admin@youweb.com', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('24', 'addr', '广东省广州市天河区88号', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('25', 'coord', '', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('26', 'thirdcode_pc', '', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('27', 'thirdcode_wap', '', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('28', 'inlet', '2', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('29', 'auto', '1', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('30', 'filter_isurl', '1', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('31', 'type', 'xml', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('32', 'index_changefreq', 'always', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('33', 'list_changefreq', 'hourly', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('34', 'view_changefreq', 'daily', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('35', 'index_priority', '1.0', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('36', 'list_priority', '0.8', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('37', 'view_priority', '0.5', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('38', 'articles_num', '100', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('39', 'zzbaidutoken', '', 'sitemap', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('40', 'app_map', 'admin', 'admin', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('41', 'status', '1', 'webfilter', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('42', 'replace', '国家级|国际级|世界级|宇宙级|千万级|百万级|星级|甲级|超甲级|完美|唯一|一流|精确|顶级|顶尖|尖端|最|国际级产品|填补国内空白|带头|首个|首次|首发|首款|首家|首选|正品|金牌|名牌|优秀|顶级|独家|唯独|独有|独创|独据|开发者|缔造者|创始|发明者|领先|领头|领导|领跑|领袖|领先|引领|创领|领航|权威|先进|耀领|缔造者|极品|极佳|顶级|尖端|顶尖|终极|绝佳|绝对|绝版|终极|极致|极具|王牌|冠军|第一|极致|永久|王牌|掌门人|领袖|独一无二|绝无仅有|前无古人|史无前例|万能|绝对|绝顶|大牌|精确|超赚|巨星|至尊|巅峰|之王|巨星|王者|冠军|资深|至尊|著名|奢侈|百分之百|国家级产品|国家免检|国家领导人|填补国内空白|史无前例|前无古人|永久|永远|万能|祖传|特效|无敌|纯天然|超赚|国际品质|专家推荐|超强|全面|全网|全球|随时结束|仅此一次|空前绝后|随时涨价|马上降价|最后一波|包治百病|药到病除|特供|专供|专家推荐|热销|畅销|全效|标杆|精确|优秀|仅此|遥遥|高档|真皮|超赚|精准|指定|精品|特供|专供|军方|军事|国旗|国徽|党旗|驰名|秒杀|一步到位|', 'webfilter', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('43', 'connector_title', '_', 'seo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('44', 'list_title', '1', 'seo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('45', 'view_title', '1', 'seo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('46', 'mobile', '138-0000-0000', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('47', 'wx', 'WX8888888', 'webinfo', '', 'cn', '0');
INSERT INTO `rrz_config` VALUES ('48', 'qrcode', '/storage/images/20200509/qr.jpg', 'webinfo', '', 'cn', '0');

-- ----------------------------
-- Table structure for `rrz_form_data`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_form_data`;
CREATE TABLE `rrz_form_data` (
  `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `form_id` int(10) unsigned DEFAULT '0' COMMENT '表单ID',
  `form_name` varchar(255) DEFAULT '' COMMENT '表单名称',
  `content` longtext COMMENT '提交内容',
  `ip` varchar(255) DEFAULT '' COMMENT '提交人IP',
  `add_time` int(10) unsigned DEFAULT NULL COMMENT '提交时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_form_id` (`form_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;


-- ----------------------------
-- Table structure for `rrz_forms`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_forms`;
CREATE TABLE `rrz_forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '表单名称',
  `config` longtext COMMENT '表单配置',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) unsigned DEFAULT '0' COMMENT '删除(0=否，1=是)',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_is_del` (`is_del`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;


-- ----------------------------
-- Table structure for `rrz_goods`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_goods`;
CREATE TABLE `rrz_goods` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '产品id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '产品名称',
  `brief` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '商品简介',
  `cat_id` mediumint(8) unsigned DEFAULT NULL COMMENT '产品分类id',
  `def_img` varchar(500) DEFAULT '' COMMENT '产品默认图片路径',
  `imgs` longtext COMMENT '产品图片集合',
  `price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '产品价格',
  `del_price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '产品划线价格',
  `store` int(8) unsigned DEFAULT '0' COMMENT '产品库存',
  `is_presell` enum('true','false') DEFAULT 'false' COMMENT '是否预售（是否定金付款）',
  `presell_price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '预售价格（定金）',
  `sku_name` varchar(500) DEFAULT '' COMMENT '规格名称',
  `sku_desc` longtext COMMENT '产品规格',
  `content` longtext COMMENT '产品详情',
  `wap_content` longtext COMMENT '手机端详情',
  `sales` int(11) unsigned DEFAULT '0' COMMENT '产品销量',
  `addtime` int(11) unsigned DEFAULT '0' COMMENT '创建时间',
  `is_head` tinyint(1) unsigned DEFAULT '0' COMMENT '头条（0=否，1=是）',
  `is_special` tinyint(1) unsigned DEFAULT '0' COMMENT '特荐（0=否，1=是）',
  `is_recom` tinyint(1) unsigned DEFAULT '0' COMMENT '推荐（0=否，1=是）',
  `is_news` tinyint(1) unsigned DEFAULT '0' COMMENT '新品（0=否，1=是）',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `sort` smallint(6) unsigned DEFAULT '100' COMMENT '排序',
  `tmpl_path` varchar(100) DEFAULT '' COMMENT '模板',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_cat_id` (`cat_id`) USING BTREE,
  KEY `idx_is_head` (`is_head`) USING BTREE,
  KEY `idx_is_special` (`is_special`) USING BTREE,
  KEY `idx_is_recom` (`is_recom`) USING BTREE,
  KEY `idx_is_news` (`is_news`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='产品管理';

-- -----------------------------
-- Records of `rrz_goods`
-- -----------------------------
INSERT INTO `rrz_goods` VALUES ('1', '产品名称一', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/ffb93cc5ab4468e598da0684facc22aa.jpg', '/storage/images/20200509/ffb93cc5ab4468e598da0684facc22aa.jpg,/storage/images/20200509/dd7e90898b9597493928614d059662d2.jpg,/storage/images/20200509/8122f5d7446a4dcafa3715b29975972b.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(0, 0, 0); font-family: sans-serif;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '    ', '0', '1588993546', '0', '0', '0', '0', '13', '100', '');
INSERT INTO `rrz_goods` VALUES ('2', '产品名称二', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/dd7e90898b9597493928614d059662d2.jpg', '/storage/images/20200509/dd7e90898b9597493928614d059662d2.jpg,/storage/images/20200509/787fb36e82bd0e7c2c0ab2a32e66f7b8.jpg,/storage/images/20200509/ffb93cc5ab4468e598da0684facc22aa.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(0, 0, 0); font-family: sans-serif;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '      ', '0', '1588993688', '0', '0', '0', '0', '11', '100', '');
INSERT INTO `rrz_goods` VALUES ('3', '产品名称三', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/787fb36e82bd0e7c2c0ab2a32e66f7b8.jpg', '/storage/images/20200509/787fb36e82bd0e7c2c0ab2a32e66f7b8.jpg,/storage/images/20200509/cebcfcea3caddd70a06a45a4f6fb9b09.jpg,/storage/images/20200509/ffb93cc5ab4468e598da0684facc22aa.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(0, 0, 0); font-family: sans-serif;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '      ', '0', '1588993813', '0', '0', '0', '0', '11', '100', '');
INSERT INTO `rrz_goods` VALUES ('4', '产品名称四', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/cebcfcea3caddd70a06a45a4f6fb9b09.jpg', '/storage/images/20200509/cebcfcea3caddd70a06a45a4f6fb9b09.jpg,/storage/images/20200509/787fb36e82bd0e7c2c0ab2a32e66f7b8.jpg,/storage/images/20200509/ffb93cc5ab4468e598da0684facc22aa.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(0, 0, 0); font-family: sans-serif;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '    ', '0', '1588993875', '0', '0', '0', '0', '18', '100', '');
INSERT INTO `rrz_goods` VALUES ('5', '产品名称五', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/3dd075f1c12ed2127ee7ed2478d1668d.jpg', '/storage/images/20200509/3dd075f1c12ed2127ee7ed2478d1668d.jpg,/storage/images/20200509/6975c3908c9aad91006869792a0e2daf.jpg,/storage/images/20200509/cebcfcea3caddd70a06a45a4f6fb9b09.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(68, 68, 68); font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-size: 14px;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '      ', '0', '1588994039', '0', '0', '0', '0', '43', '100', '');
INSERT INTO `rrz_goods` VALUES ('6', '产品名称六', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/6975c3908c9aad91006869792a0e2daf.jpg', '/storage/images/20200509/6975c3908c9aad91006869792a0e2daf.jpg,/storage/images/20200509/3dd075f1c12ed2127ee7ed2478d1668d.jpg,/storage/images/20200509/8c7a2369a5e1d4a9efa9a96ecebd26fd.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(0, 0, 0); font-family: sans-serif;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '    ', '0', '1588994116', '0', '0', '0', '0', '12', '100', '');
INSERT INTO `rrz_goods` VALUES ('7', '产品名称七', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/6d4b8d4499f02590833145c0d555867d.jpg', '/storage/images/20200509/6d4b8d4499f02590833145c0d555867d.jpg,/storage/images/20200509/8c7a2369a5e1d4a9efa9a96ecebd26fd.jpg,/storage/images/20200509/6975c3908c9aad91006869792a0e2daf.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(0, 0, 0); font-family: sans-serif;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '    ', '0', '1588994188', '0', '0', '0', '0', '13', '100', '');
INSERT INTO `rrz_goods` VALUES ('8', '产品名称八', '景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的', '1', '/storage/images/20200509/08969355ca564ccfa853784bcd722aa1.jpg', '/storage/images/20200509/08969355ca564ccfa853784bcd722aa1.jpg,/storage/images/20200509/6d4b8d4499f02590833145c0d555867d.jpg,/storage/images/20200509/8c7a2369a5e1d4a9efa9a96ecebd26fd.jpg', '0.00', '0.00', '999', 'false', '0.00', '', '', '<p><span style=\"color: rgb(0, 0, 0); font-family: sans-serif;\">景观设计是提升的一种途径;是传递对生命的感悟和对美好生活的向往的表达方式，多样化的空间组合，多元化的场验，延展化的幸福满溢推敲裁剪，对自然的尊重模数化切割组合材质，彰显材料本身的性格表现;景观设计是技术之上的艺术，是源于对大自然的热爱和理解基于场地的地形地貌，对自然元素的提炼、抽象和解构重组。追求“活力”与“生态”的高品质设计,坚持“有机，创新，和谐”的文化艺术融合.</span></p>', '    ', '0', '1588994236', '0', '0', '0', '0', '21', '100', '');

-- ----------------------------
-- Table structure for `rrz_goods_attr`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_goods_attr`;
CREATE TABLE `rrz_goods_attr` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品属性id自增',
  `goods_id` bigint(18) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `attr_value` text COMMENT '属性值',
  `attr_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '属性id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `goods_id` (`goods_id`) USING BTREE,
  KEY `attr_id` (`attr_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='产品扩展属性关联表';


-- ----------------------------
-- Table structure for `rrz_goods_attribute`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_goods_attribute`;
CREATE TABLE `rrz_goods_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '属性id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '属性名称',
  `is_filter` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否应用于条件筛选',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型（0 单行文本框 1下拉式列表 2多行文本框）',
  `values` text COMMENT '属性值集合',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '100' COMMENT '属性排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='产品扩展属性';


-- ----------------------------
-- Table structure for `rrz_goods_cat`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_goods_cat`;
CREATE TABLE `rrz_goods_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属模型id',
  `name` varchar(255) DEFAULT '' COMMENT '分类名称',
  `dir_name` varchar(255) DEFAULT '' COMMENT '目录英文名',
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '父级ID',
  `img` varchar(255) DEFAULT '' COMMENT '分类图片',
  `id_path` varchar(255) NOT NULL DEFAULT '' COMMENT 'id路径',
  `depth` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '节点深度',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '分类排序',
  `path` varchar(255) DEFAULT '' COMMENT '节点路径',
  `tmpl_path` varchar(100) DEFAULT '' COMMENT '模板',
  `tmpl_view` varchar(100) DEFAULT '' COMMENT '文档模板',
  `seo_title` varchar(255) DEFAULT '' COMMENT 'SEO标题',
  `seo_description` mediumtext COMMENT '分类描述',
  `seo_keywords` varchar(200) DEFAULT '' COMMENT '搜索关键词',
  `attrs` longtext COMMENT '参数列表',
  `uptime` int(10) unsigned DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_parent_id` (`parent_id`) USING BTREE,
  KEY `idx_channel_id` (`channel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='产品分类';

-- -----------------------------
-- Records of `rrz_goods_cat`
-- -----------------------------
INSERT INTO `rrz_goods_cat` VALUES ('1', '0', '策划运营', '', '0', '', '1', '1', '1', '1001', '', '', '', '', '', '', '1588993170');
INSERT INTO `rrz_goods_cat` VALUES ('2', '0', '规划设计', '', '0', '', '2', '1', '2', '1002', '', '', '', '', '', '', '1588993181');
INSERT INTO `rrz_goods_cat` VALUES ('3', '0', '工程施工', '', '0', '', '3', '1', '3', '1003', '', '', '', '', '', '', '1588993186');
INSERT INTO `rrz_goods_cat` VALUES ('4', '0', '工程养护', '', '0', '', '4', '1', '4', '1004', '', '', '', '', '', '', '1588993192');
INSERT INTO `rrz_goods_cat` VALUES ('5', '0', '苗木产销', '', '0', '', '5', '1', '5', '1005', '', '', '', '', '', '', '1588993198');

-- ----------------------------
-- Table structure for `rrz_goods_skus`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_goods_skus`;
CREATE TABLE `rrz_goods_skus` (
  `sku_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '产品规格id',
  `goods_id` bigint(18) unsigned NOT NULL DEFAULT '0' COMMENT '产品id',
  `price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '产品价格',
  `sku_name` varchar(500) DEFAULT '' COMMENT '规格名称',
  `sku_desc` longtext COMMENT '规格明细',
  PRIMARY KEY (`sku_id`) USING BTREE,
  KEY `idx_goods_id` (`goods_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='产品规格详情';


-- ----------------------------
-- Table structure for `rrz_goods_spec_values`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_goods_spec_values`;
CREATE TABLE `rrz_goods_spec_values` (
  `spec_value_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规格值ID',
  `spec_id` bigint(15) unsigned NOT NULL DEFAULT '0' COMMENT '规格ID',
  `spec_value` varchar(100) NOT NULL DEFAULT '' COMMENT '规格值',
  PRIMARY KEY (`spec_value_id`) USING BTREE,
  KEY `idx_spec_id` (`spec_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='规格值';


-- ----------------------------
-- Table structure for `rrz_goods_specification`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_goods_specification`;
CREATE TABLE `rrz_goods_specification` (
  `spec_id` bigint(15) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `spec_name` varchar(50) DEFAULT NULL COMMENT '规格名',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序顺序',
  PRIMARY KEY (`spec_id`) USING BTREE,
  KEY `idx_sort` (`sort`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='规格表';


-- ----------------------------
-- Table structure for `rrz_search_keywords`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_search_keywords`;
CREATE TABLE `rrz_search_keywords` (
  `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '表ID',
  `keywords` text COMMENT '关键词',
  `type` varchar(50) DEFAULT 'article' COMMENT '搜索类型',
  `ip` varchar(100) DEFAULT NULL COMMENT '搜索客户端的ip地址',
  `hot` int(10) unsigned DEFAULT '0' COMMENT '关键词搜索量',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '搜索时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_hot` (`hot`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;


-- ----------------------------
-- Table structure for `rrz_site_links`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_site_links`;
CREATE TABLE `rrz_site_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT '' COMMENT '网站标题',
  `url` varchar(255) DEFAULT '' COMMENT '网站地址',
  `logo` varchar(255) DEFAULT '' COMMENT '网站LOGO',
  `sort` int(10) unsigned DEFAULT '0' COMMENT '排序号',
  `target` tinyint(1) unsigned DEFAULT '0' COMMENT '是否新窗口打开',
  `email` varchar(50) DEFAULT '',
  `intro` text COMMENT '网站简况',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(1=显示，0=屏蔽)',
  `lang` varchar(50) DEFAULT 'cn' COMMENT '语言标识',
  `add_time` int(11) unsigned DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) unsigned DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='友情链接表';

-- -----------------------------
-- Records of `rrz_site_links`
-- -----------------------------
INSERT INTO `rrz_site_links` VALUES ('1', '百度', 'http://www.baidu.com', '', '100', '1', '', '', '1', 'cn', '1588753882', '1588753882');
INSERT INTO `rrz_site_links` VALUES ('2', '腾讯', 'http://www.qq.com', '', '100', '1', '', '', '1', 'cn', '1588757202', '1588757202');
INSERT INTO `rrz_site_links` VALUES ('3', '新浪', 'http://www.sina.com.cn', '', '100', '1', '', '', '1', 'cn', '1589024479', '1589024479');
INSERT INTO `rrz_site_links` VALUES ('4', '淘宝', 'http://www.taobao.com', '', '100', '1', '', '', '1', 'cn', '1589024491', '1589024491');
INSERT INTO `rrz_site_links` VALUES ('5', '微博', 'http://www.weibo.com', '', '100', '1', '', '', '1', 'cn', '1589024503', '1589024503');

-- ----------------------------
-- Table structure for `rrz_site_menus`
-- -----------------------------
DROP TABLE IF EXISTS `rrz_site_menus`;
CREATE TABLE `rrz_site_menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `dir_name` varchar(255) DEFAULT '' COMMENT '目录英文名',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单',
  `id_path` varchar(255) NOT NULL DEFAULT '' COMMENT 'id路径',
  `depth` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '菜单深度',
  `url` varchar(200) DEFAULT '' COMMENT '自定义链接',
  `sort` smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `path` varchar(100) NOT NULL DEFAULT '' COMMENT '上下级路径',
  `target_blank` enum('true','false') NOT NULL DEFAULT 'false' COMMENT '是否新开窗口',
  `config` longtext COMMENT '配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='导航菜单表';

-- -----------------------------
-- Records of `rrz_site_menus`
-- -----------------------------
INSERT INTO `rrz_site_menus` VALUES ('1', '关于我们', 'about', '0', '1', '1', '/article/1.html', '1', '1001', 'false', '');
INSERT INTO `rrz_site_menus` VALUES ('2', '新闻动态', 'news', '0', '2', '1', '/node/2.html', '2', '1002', 'false', '');
INSERT INTO `rrz_site_menus` VALUES ('3', '产品中心', '', '0', '3', '1', '/cats.html', '3', '1003', 'false', '');
INSERT INTO `rrz_site_menus` VALUES ('4', '成功案例', 'chenggonganli', '0', '4', '1', '/node/3.html', '4', '1004', 'false', '');
INSERT INTO `rrz_site_menus` VALUES ('5', '联系我们', 'lianxiwomen', '0', '5', '1', '/article/16.html', '5', '1005', 'false', '');
INSERT INTO `rrz_site_menus` VALUES ('6', '公司简介', '', '1', '1,6', '2', '/article/1.html', '1', '10011001', 'false', '');
INSERT INTO `rrz_site_menus` VALUES ('7', '企业文化', '', '1', '1,7', '2', '/article/2.html', '2', '10011002', 'false', '');
