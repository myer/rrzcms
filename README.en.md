# rrzcms（企业_商城_小程序_人人站CMS）

#### Description
人人站CMS是基于TP6.0框架为核心开发的免费+开源的企业内容管理系统，专注企业建站用户需求。提供海量各行业模板，降低中小企业网站建设、网络营销成本，致力于打造用户舒适的建站体验。这是一套安全、简洁、易用、免费的流行CMS，包含完整后台管理、前台展示，直接下载安装即可使用。 演示网址：http://demo.rrzcms.com 官方网站：http://www.rrzcms.com

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
