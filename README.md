# rrzcms（企业_商城_小程序_人人站CMS）

#### 人人站介绍
人人站CMS是基于TP6.0框架为核心开发的免费+开源的企业内容管理系统，专注企业建站用户需求。提供海量各行业模板，降低中小企业网站建设、网络营销成本，致力于打造用户舒适的建站体验。这是一套安全、简洁、易用、免费的流行CMS，包含完整后台管理、前台展示，直接下载安装即可使用。 演示网址：http://demo.rrzcms.com 官方网站：http://www.rrzcms.com

#### 人人站架构
这是基于ThinkPHP6.0+小程序+Layui研发的一套简易CMS企业建站系统，包含完整的后台管理和前台展示。


#### 后台截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0830/140221_0306cfbf_9653122.png "QQ图片20210830140112.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0830/140241_23b7d64b_9653122.png "QQ图片20210830140134.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0830/140252_e23dbdac_9653122.png "QQ图片20210830140202.png")

#### 海量模板

![输入图片说明](https://images.gitee.com/uploads/images/2021/0830/140407_f49810a7_9653122.png "QQ图片20210830140330.png")

#### 海量插件

![输入图片说明](https://images.gitee.com/uploads/images/2021/0830/140421_200d0f43_9653122.png "QQ图片20210830140343.png")

#### 效果演示

1.  前台演示：http://demo.rrzcms.com
2.  后台演示：http://demo.rrzcms.com/admin
3.  Demo下载：http://www.rrzcms.com/Index/download.html
4.  免费模板：http://www.rrzcms.com/Index/themes.html

#### 安装教程

1.  新手必读：http://www.rrzcms.com/newsinfo/1.html
2.  常见问题：http://www.rrzcms.com/newsinfo/46.html

#### 参与贡献

1.  人人站团队


