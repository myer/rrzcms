<?php
/**
 * 人人站CMS
 * ============================================================================
 * 版权所有 2015-2030 山东康程信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.rrzcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */

/**
 * 获取url根目录
 * @return mixed|string
 */
function getRootUrl($domain = false) {
    $file = request()->baseFile($domain);
    $url = str_replace(['/public/index.php', '/index.php'], '', $file);
    $url = rtrim($url, '/') . '/';
    return $url;
}

/*
 * 获取当前app 名称
 * @return string
 */
function getAppName($name = '') {
    $name = $name ?: app()->http->getName();
    $map = array_flip(C('app.app_map'));
    return $map[strtolower($name)] ?? $name;
}

/**
 * 获取当前app根目录的url
 * @return string
 */
function getAppRootUrl($name = '') {
    $name = getAppName($name);
    return U('/' . $name, [], false);
}

/**
 *  获取 人人站CMS 地址
 * @param string $url
 * @param string $dir_name
 * @param bool $domain
 * @return mixed|string
 */
function getRrzUrl($url = '', $dir_name = '', $domain = false) {
    $lang = I('param.lang');
    $path = $lang ? '/' . $lang : '';

    if (in_array($url, ['/search', '/formSubmit', '/brand'])) {
        return U($path . $url, [], true, $domain);
    }

    $libMenus = new \app\home\lib\Menus;
    $info = $libMenus->getPageInfo($url);
    if ($info['page'] == 'url' || !$info['page']) return $url;
    if ($info['page'] == 'index') {
        return $lang ? U($path, [], false, $domain) : getRootUrl($domain);
    }

    if (!$dir_name && $dir_name !== null) {
        $app = app();
        $dir_name = M('site_menus')->where('url', 'like', '%' . $url)->cache($app->isDebug() == false)
            ->order('depth desc')->value('dir_name');
    }
    if ($dir_name) {
        return U($path . "/{$dir_name}", [], true, $domain);
    }
    if ($info['page'] == 'cat' && $info['pageId'] == 0) {
        return U($path . "/cats", [], true, $domain);
    }
    return U($path . "/{$info['page']}/{$info['pageId']}", [], true, $domain);
}


/**
 * 价格处理
 * @param int $price
 * @param bool $Int
 * @return float|int
 */
function price_format($price = 0, $Int = true) {
    $price = $price ? $price : 0;
    if ($Int && (int)$price == $price) {
        return (int)$price;
    }
    return round($price, 2);
}


/**
 * 判断url是否完整的链接
 * @param  string $url 网址
 * @return boolean
 */
function is_http_url($url) {
    preg_match("/^((\w)*:)?(\/\/).*$/", $url, $match);
    if (empty($match)) {
        return false;
    } else {
        return true;
    }
}

/**
 * 获取随机字符串
 * @param int $randLength 长度
 * @param int $addtime 是否加入当前时间戳
 * @param int $includenumber 是否包含数字
 * @return string
 */
function get_rand_str($randLength = 6, $addtime = 1, $includenumber = 0) {
    if (1 == $includenumber) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQEST123456789';
    } else if (2 == $includenumber) {
        $chars = '123456789';
    } else {
        $chars = 'abcdefghijklmnopqrstuvwxyz';
    }
    $len = strlen($chars);
    $randStr = '';
    for ($i = 0; $i < $randLength; $i++) {
        $randStr .= $chars[rand(0, $len - 1)];
    }
    $tokenvalue = $randStr;
    if ($addtime) {
        $tokenvalue = $randStr . time();
    }
    return $tokenvalue;
}

/**
 * 递归删除文件夹
 * @param string $path 目录路径
 * @param boolean $delDir 是否删除空目录
 * @return boolean
 */
function delFile($path, $delDir = false) {
    $handle = is_dir($path) ? @opendir($path) : false;
    if ($handle) {
        while (false !== ($item = readdir($handle))) {
            if ($item != "." && $item != "..")
                is_dir("$path/$item") ? delFile("$path/$item", $delDir) : @unlink("$path/$item");
        }
        closedir($handle);
        if ($delDir) {
            return @rmdir($path);
        }
    } else {
        if (file_exists($path)) {
            return @unlink($path);
        } else {
            return false;
        }
    }
}


/**
 * 获取文章内容html中第一张图片地址
 *
 * @param  string $html html代码
 * @return boolean
 */
function get_html_first_imgurl($html) {
    $pattern = '~<img [^>]*[\s]?[\/]?[\s]?>~';
    preg_match_all($pattern, $html, $matches);//正则表达式把图片的整个都获取出来了
    $img_arr = $matches[0];//图片
    $first_img_url = "";
    if (!empty($img_arr)) {
        $first_img = $img_arr[0];
        $p = "#src=('|\")(.*)('|\")#isU";//正则表达式
        preg_match_all($p, $first_img, $img_val);
        if (isset($img_val[2][0])) {
            $first_img_url = $img_val[2][0]; //获取第一张图片地址
        }
    }

    return $first_img_url;
}

/**
 * 字符串截取，支持中文和其他编码
 * @param string $str 需要转换的字符串
 * @param int $start 开始位置
 * @param null $length 截取长度
 * @param bool $suffix 截断显示字符
 * @param string $charset 编码格式
 * @return string
 */
function msubstr($str = '', $start = 0, $length = null, $suffix = false, $charset = "utf-8") {
    if (function_exists("mb_substr")) {
        $slice = mb_substr($str, $start, $length, $charset);
    } elseif (function_exists('iconv_substr')) {
        $slice = iconv_substr($str, $start, $length, $charset);
        if (false === $slice) {
            $slice = '';
        }
    } else {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        $slice = join("", array_slice($match[0], $start, $length));
    }

    $str_len = strlen($str); // 原字符串长度
    $slice_len = strlen($slice); // 截取字符串的长度
    if ($slice_len < $str_len) {
        $slice = $suffix ? $slice . '...' : $slice;
    }

    return $slice;
}


/**
 * 自定义只针对htmlspecialchars编码过的字符串进行解码
 * @param string $str 需要转换的字符串
 * @return string
 */
function rrz_htmlspecialchars_decode($str = '') {
    if (is_string($str) && stripos($str, '&lt;') !== false && stripos($str, '&gt;') !== false) {
        $str = htmlspecialchars_decode($str);
    }
    return $str;
}

/**
 * 截取内容清除html之后的字符串长度，支持中文和其他编码
 * @param string $str 需要转换的字符串
 * @param int $start 开始位置
 * @param null $length 截取长度
 * @param bool $suffix 截断显示字符
 * @param string $charset 编码格式
 * @return string
 */
function html_msubstr($str = '', $start = 0, $length = null, $suffix = false, $charset = "utf-8") {
    if (false) {
        $length = $length * 2;
    }
    $str = rrz_htmlspecialchars_decode($str);
    $str = checkStrHtml($str);
    return msubstr($str, $start, $length, $suffix, $charset);
}

/**
 * html 转 文本 截取
 * @param string $str
 * @param null $length
 * @param int $start
 * @param bool $suffix
 * @param string $charset
 * @return string
 */
function html2text($str = '', $length = null, $start = 0, $suffix = true, $charset = "utf-8") {
    return html_msubstr($str, $start, $length, $suffix, $charset);
}

function subtext($str = '', $length = null, $start = 0, $suffix = true, $charset = "utf-8") {
    return text_msubstr($str, $length, $start, $suffix, $charset);
}

/**
 * 针对多语言截取，其他语言的截取是中文语言的2倍长度
 * @param string $str
 * @param null $length
 * @param int $start
 * @param bool $suffix
 * @param string $charset
 * @return string
 */
function text_msubstr($str = '', $length = null, $start = 0, $suffix = false, $charset = "utf-8") {
    if (false) {
        $length = $length * 2;
    }
    return msubstr($str, $start, $length, $suffix, $charset);
}

/**
 * 过滤Html标签
 *
 * @param     string $string 内容
 * @return    string
 */
function checkStrHtml($string) {
    $string = trim_space($string);

    if (is_numeric($string)) return $string;
    if (!isset($string) or empty($string)) return '';
    $string = preg_replace('/[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F]/', '', $string);
    $string = ($string);
    $string = strip_tags($string, ""); //清除HTML如<br />等代码
    $string = str_replace(["\n", "\t", PHP_EOL, "\r"], "", $string);//去掉换行、制表符号、回车

    $string = str_replace("'", "‘", $string); //替换单引号
    $string = str_replace("&amp;", "&", $string);
    $string = str_replace("&nbsp;", "", $string);

    // --过滤微信表情
    $string = preg_replace_callback('/[\xf0-\xf7].{3}/', function ($r) {
        return '';
    }, $string);

    return $string;
}

/**
 * 过滤前后空格等多种字符
 *
 * @param string $str 字符串
 * @param array $arr 特殊字符的数组集合
 * @return string
 */
function trim_space($str, $arr = array()) {
    if (empty($arr)) {
        $arr = array(' ', '　');
    }
    foreach ($arr as $key => $val) {
        $str = preg_replace('/(^' . $val . ')|(' . $val . '$)/', '', $str);
    }

    return $str;
}

/**
 * 格式化字节大小
 *
 * @param  number $size 字节数
 * @param  string $delimiter 数字和单位分隔符
 * @return string            格式化后的带单位的大小
 */
function format_bytes($size, $delimiter = '') {
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $size >= 1024 && $i < 5; $i++) $size /= 1024;
    return round($size, 2) . $delimiter . $units[$i];
}

/**
 * 验证是否shell注入
 * @param mixed $data 任意数值
 * @return mixed
 */
function preventShell($data = '') {
    $data = true;
    if (is_string($data) && (preg_match('/^phar:\/\//i', $data) || stristr($data, 'phar://'))) {
        $data = false;
    } else if (is_numeric($data)) {
        $data = intval($data);
    }

    return $data;
}

/**
 * 显示指定插件内容
 * @param string $code    插件编号
 * @return string
 */
function pluginexec($code)
{
    $info = "";
    $status = M('plugin')->where("code", $code)->value('status');
    if ($status == 1) {
        $class = \app\plugin\lib\Common::plugin_get_class($code);
        if (class_exists($class)) {
            $plugin = new $class;
            $info = $plugin->getHtml();
        }
    }
    echo $info;
}
//远程图片本地化
function remoteimg_tolocal($content,$img='')
{
    $imgList = array();
    if($img) {
        $imgList[] = $img;
        $content = $img;
    }
    else {
        preg_match_all("/src=[\"|'|\s]([^\"|^\'|^\s]*?)/isU", $content, $imgList);
        $imgList = array_unique($imgList[1]);
    }

    $filesystem = \think\facade\Filesystem::class;
    $config = $filesystem::getDiskConfig($filesystem::getDefaultDriver());
    $imgdir= DIRECTORY_SEPARATOR . 'images'. DIRECTORY_SEPARATOR .date('Ymd').DIRECTORY_SEPARATOR;
    $filedir = $config['root'].$imgdir;
    $urldir=$config['url'].$imgdir;
    $urldir=str_replace('\\', '/', $urldir);
    is_dir($filedir) or mkdir($filedir, 0777, true);

    $basedomain=request()->domain();
    foreach($imgList as $imgurl) {
        $imgurl = trim($imgurl);

        // 本站图片
        if (preg_match("#" . $basedomain . "#i", $imgurl)) {
            continue;
        }
        if (strpos($imgurl, '//') === 0)
            $imgurl = 'https:' . $imgurl;
        // 不是合法链接
        if (!preg_match("#^http(s?):\/\/#i", $imgurl)) {
            continue;
        }

        $heads = @get_headers($imgurl, 1);

        // 获取请求头并检测死链
        if (empty($heads)) {
            continue;
        } else if (!(stristr($heads[0], "200") && !stristr($heads[0], "304"))) {
            continue;
        }
        // 图片扩展名
        $fileType = substr($heads['Content-Type'], -4, 4);
        if (!preg_match("#\.(jpg|jpeg|gif|png|ico|bmp|webp|svg)#i", $fileType)) {
            if ($fileType == 'image/gif') {
                $fileType = ".gif";
            } else if ($fileType == 'image/png') {
                $fileType = ".png";
            } else if ($fileType == 'image/x-icon') {
                $fileType = ".ico";
            } else if ($fileType == 'image/bmp') {
                $fileType = ".bmp";
            } else if ($fileType == 'image/webp') {
                $fileType = ".webp";
            } else if ($heads['Content-Type'] == 'image/svg+xml') {
                $fileType = ".svg";
            } else {
                $fileType = '.jpg';
            }
        }
        $fileType = strtolower($fileType);

        //打开输出缓冲区并获取远程图片
        ob_start();
        $context = stream_context_create(array('http' => array('follow_location' => false)));
        readfile($imgurl, false, $context);
        $img = ob_get_contents();
        ob_end_clean();
        preg_match("/[\/]([^\/]*)[\.]?[^\.\/]*$/", $imgurl, $m);

        $file = [];
        $file['oriName'] = $m ? $m[1] : "";
        $file['filesize'] = strlen($img);
        $file['ext'] = $fileType;
        $file['name'] = md5((string)microtime(true)) . $file['ext'];
        $file['fullName'] = $filedir . $file['name'];
        $fullName = $file['fullName'];

        //检查文件大小是否超出限制
        if ($file['filesize'] >= 20480000) {
            continue;
        }
        //移动文件
        if (!(file_put_contents($fullName, $img) && file_exists($fullName))) { //移动失败
            continue;
        }
        $content = str_replace($imgurl, $urldir . $file['name'], $content);
    }
    return $content;
}