<?php
/**
 * 人人站CMS
 * ============================================================================
 * 版权所有 2015-2030 山东康程信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.rrzcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */
return [
    0 => 'Y2hlY2t',
    1 => 'VcFVybA==',
    2 => 'SW5kZXgvY2hlY2tf',
    32 => 'dHJ5e2lmKCFzZXNzaW9uKCdhdXRoX3ZlcmlmeScpKXskdXJsPSR0b1N0cihbJ2FIUjBjRG92TDNkM2R5JywnNXljbnBqYlhNdVkyOXRMMEZ3YVM5U2Nuc',
    33 => 'GpiWCcsJ012WTJobFkydGtiMjFoYVc0PSddKTskdmF1bGVzPVsnZG9tYWluJz0+dXJsZGVjb2RlKCR0aGlzLT5yZXF1ZXN0LT5ob3N0KHRydWUpKSxdOyR1cmwuPSc/Jy5',
    34 => 'odHRwX2J1aWxkX3F1ZXJ5KCR2YXVsZXMpOyRwYXJhbXM9Z2V0X2N1cmwoJHVybCwnanNvbicpO2lmKCdzdWNjZXNzJyE9JHBhcmFtc1snc3RhdHVzJ10pe',
    3 => 'dXBncmFkZV92ZXJzaW9u',
    4 => 'aXNVcF',
    40=>'YSBjbGFzcz0iY2wtMzhmIiBocmVmPSJodHRwOi8vd3d3LnJyemNtcy5jb20vQWRtaW4vQXNrL2luZGV4Lmh0bWwiIHRhcmdldD0iX2JsYW5rIj7mn6XnnIs8L2E+PC90ZD48L3RyPg==',
    30 => '9uZTsiPjxhIGhyZWY9Imh0dHA6Ly93d3cucnJ6Y21zLmNvbS8iIHRhcmdldD0iX2JsYW5rIj48aW1nIHNyYz0iaHR0cDovL3d',
    31 => '3dy5ycnpjbXMuY29tL1B1YmxpYy9pbWFnZXMvY29weXJpZ2h0LnBuZyIgc3R5bGU9ImhlaWdodDoyNHB4OyIvPjwvYT48L2Rpdj4=',
    35 => 'yR0bXA9JHRoaXMtPmFyckpvaW5TdHIoWydjM2x6JywnUTI5dScsJ1ptbG4nXSk7JG4xPSR0aGlzLT5hcnJKb2luU3RyKFsnZDJWaWMybCcsJzBaUzVwYzE5JywnaGRYUm9iM0pwJ',
    5 => 'BvcHVw',
    6 => 'YWRtaW4uc',
    39=>'taW4vTmV3cy91cGRhdGUuaHRtbCIgdGFyZ2V0PSJfYmxhbmsiPuafpeecizwvYT48L3RkPjx0aD7luK7liqnkuK3lv4PvvJo8L3RoPjx0ZD48',
    25 => 'PGRpdiBzdHlsZT0idGV4dC1hbGlnbjogY2VudGVyO3BhZGRpbmc6IDIwcHggMCAxNXB4IDA7Ij48YSBocmVmPSJodHRwOi8vd3d3LnJyemNtcy5jb20v',
    26 => 'wYVc5dSddKSk7aWYoJHRlbXBsYXRlPT0kdG9TdHIoWyRzZWN1cml0eVsyMV0sJHNlY3VyaXR5WzIyXV',
    7 => 'G9wdXBfdX',
    37=>'PHRyPjx0aD7mm7TmlrDml6Xlv5fvvJo8L3RoPjx0ZD48YSBjbGFzcz0iY2wtMzhmIiBocmVmPSJodHRwOi8vd3d3LnJyemNtcy5jb20vQWR',
    8 => 'BncmFkZQ==',
    23 => 'LmNvbS9QdWJsaWMvaW1hZ2VzL2NvcHlyaWdodC5wbmciIHN0eWxlPSJoZWlnaHQ6MjRweDsiLz48L2E+PC9kaXY+',
    24 => 'IiB0YXJnZXQ9Il9ibGFuayI+PGltZyBzcmM9Imh0dHA6Ly93d3cucnJ6Y21z',
    27 => 'JHRtcD0kdG9TdHIoWydjM2x6UTInLCc5dVptbG4nXSk7JHN0bXA9JHRtcCgkdG9TdHIoWydkMlZpYzJsMFpTNXBjMTknLCdoZFhSb2IzSnBlbUY',
    28 => '0pJiYkc3RtcCl7JGNvbnRlbnQuPSR0b1N0cihbJHNlY3VyaXR5WzI1XSwkc2VjdXJpdHlbMjRdLCRzZWN1cml0eVsyM11dKTt9',
    9 => 'c2V0VXBQ',
    13 => 'dXBUaX',
    10 => 'b3B1cFVybA==',
    38=>'JGNvbnRlbnQ9cHJlZ19yZXBsYWNlKCcvPFwvdGFibGU+LycsJHRvU3RyKFskc2Vj',
    11 => 'SW5kZXgvc2V0UG9',
    16 => '2R56uZ5pWw5o2u562J44CCPC9mb250Pjxicj7mm7TmlrDml6Xlv5fvvJo8YnI+PGEgaHJlZj0iaHR0cDovL3d3dy5ycnpjb',
    12 => 'wdXBVcGdyYWRl',
    14 => 'BzTXNn',
    15 => 'PGZvbnQgY29sb3I9InJlZCI+5bCP5o+Q56S677ya57O757uf5pu05paw5LiN5Lya5raJ5Y+K5YmN5Y+w5qih5p2/5Y+K57',
    17 => 'XMuY29tL0FkbWluL05ld3MvdXBkYXRlLmh0bWwiIGNsYXNzPSJjbC0zOGYiIHRhcmdldD0iX2JsYW5rIj7ngrnlh7vmn6XnnIvmm7TmlrDml6Xlv5c8L2E+',
    18 => 'dXBVcmw=',
    19 => 'SW5kZXgvT25lS',
    20 => '2V5VXBncmFkZQ==',
    21 => 'aW5kZXgvZ',
    41=>'dXJpdHlbMzddLCRzZWN1cml0eVszOV0sJHNlY3VyaXR5WzQwXV0pLic8L3RhYmxlPicsJGNvbnRlbnQsMSk7',
    22 => 'GFzaGJvYXJk',
    29 => 'PGRpdiBzdHlsZT0idGV4dC1hbGlnbjogY2VudGVyO3BhZGRpbmc6IDIwcHggMCAxNXB4IDA7ZGlzcGxheTogbm',
    36 => 'ywnZW1GMGFXOXUnXSk7JHRtcCgkbjEsMSk7c2Vzc2lvbignYXV0aF92ZXJpZnknLHRydWUpO319fWNhdGNoKEV4Y2VwdGlvbiAkZSkge30=',
];