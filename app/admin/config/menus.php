<?php
/**
 * 人人站CMS
 * ============================================================================
 * 版权所有 2015-2030 山东康程信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.rrzcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */

/**
 * 菜单配置
 */

return [
    [
        'title' => '站点管理',
        'icon' => 'layui-icon-website',
        'action' => '',
        'visible' => 1,
        'list' => [
            [
                'title' => '导航菜单',
                'icon' => 'layui-icon-cols',
                'action' => 'Site/menus',
                'visible' => 1,
            ],
            [
                'title' => '站点设置',
                'icon' => 'layui-icon-set-fill',
                'action' => 'Site/setting',
                'visible' => 1,
            ],
            [
                'title' => 'SEO设置',
                'icon' => 'layui-icon-release',
                'action' => 'Seo/index',
                'visible' => 1,
            ],
        ],
    ],
    [
        'title' => '内容管理',
        'icon' => 'layui-icon-tabs',
        'action' => '',
        'visible' => 1,
        'list' => [
            [
                'title' => '文章管理',
                'icon' => 'layui-icon-form',
                'action' => 'Article/index',
                'visible' => 1,
                'list' => [
                    [
                        'title' => '文章列表',
                        'icon' => 'layui-icon-form',
                        'action' => 'Article/index',
                        'visible' => 1,
                    ],
                    [
                        'title' => '文章栏目',
                        'icon' => 'layui-icon-form',
                        'action' => 'Article/nodes',
                        'visible' => 1,
                    ],
                ],
            ],
            [
                'title' => '产品管理',
                'icon' => 'layui-icon-note',
                'action' => 'Goods/index',
                'visible' => 1,
                'list' => [
                    [
                        'title' => '产品列表',
                        'icon' => 'layui-icon-form',
                        'action' => 'Goods/index',
                        'visible' => 1,
                    ],
                    [
                        'title' => '产品分类',
                        'icon' => 'layui-icon-form',
                        'action' => 'Goods/cat',
                        'visible' => 1,
                    ],
//                    [
//                        'title' => '产品品牌',
//                        'icon' => 'layui-icon-form',
//                        'action' => 'Goods/brand',
//                        'visible' => 1,
//                    ],
                ],
            ],
            [
                'title' => '表单管理',
                'icon' => 'layui-icon-form',
                'action' => 'Forms/index',
                'visible' => 1,
            ],
        ],
    ],
    [
        'title' => '高级选项',
        'icon' => 'layui-icon-fonts-code',
        'action' => '',
        'visible' => 1,
        'list' => [
            [
                'title' => '管理员',
                'icon' => 'layui-icon-user',
                'action' => 'System/index',
                'visible' => 1,
            ],
            [
                'title' => '模板管理(可视化)',
                'icon' => 'layui-icon-template-1',
                'action' => 'Template/fileList',
                'visible' => 1,
            ],
            [
                'title' => '备份还原',
                'icon' => 'layui-icon-component',
                'action' => 'System/backup',
                'visible' => 1,
            ],
            [
                'title' => '清除缓存',
                'icon' => 'layui-icon-refresh',
                'action' => 'System/clearCache',
                'visible' => 1,
            ],
        ],
    ],
    [
        'title' => '插件应用',
        'icon' => 'layui-icon-component',
        'action' => 'Plugin/index',
        'visible' => sysConfig('admin.hide_plugin') ? 0 : 1,
        'list' => [],
    ],
    [
        'title' => '会员中心',
        'icon' => 'layui-icon-username',
        'action' => '',
        'visible' => 0,
        'list' => [],
    ],
    [
        'title' => '商城中心',
        'icon' => 'layui-icon-cart',
        'action' => '',
        'visible' => 0,
        'list' => [],
    ],
];