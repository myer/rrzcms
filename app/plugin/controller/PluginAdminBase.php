<?php
/**
 * 人人站CMS
 * ============================================================================
 * 版权所有 2015-2030 山东康程信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.rrzcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */

namespace app\plugin\controller;


class PluginAdminBase extends PluginBase
{
    //初始化
    protected function initialize()
    {
        $account = getAccount("admin");
        if (!$account) {
            if ($this->request->isAjax()) {
                $this->request->isJson() or exit('no_login');
                exit(json_encode(['code' => 'no_login',]));
            } else {
                $this->redirect(U('Login/index'));
            }
            return;
        }
    }
    /*
     * 显示带有tab标签的页面
     * @param $tabcode 当前tab标签编码
     * @param $temp 要渲染的模板
     */
    function tab_fetch($tabcode,$temp)
    {
        $plugin = $this->getPlugin();
        $config = $plugin->info['config'];
        if (!isset($config['tabs']) || !$config['tabs'])
            $config['tabs'] = [['code' => 'index', 'name' => $plugin->info['name'], 'url' => 'Admin/index', 'target' => 'page', 'urltype' => 1]];

        $tabhtml = '<div class="layui-card"><div class="layui-card-header layui-tab layui-tab-brief p0 bd-w0"><ul class="layui-tab-title">';
        $tabhtml .= '<li target="page" href="' . U('Plugin/index') . '">我的插件</li>';
        foreach ($config['tabs'] as $tinfo) {
            if ($tinfo['urltype'] == 1)
                $tinfo['url'] = $this->getUrl($tinfo['url']);
            if (!$tinfo['target'] || $tinfo['target'] == 'page')
                $tabhtml .= '<li ' . ($tinfo['code'] == $tabcode ? 'class="layui-this"' : ('target="page" href="' . $tinfo['url'] . '"')) . '>' . $tinfo['name'] . '</li>';
            else
                $tabhtml .= '<li><a href="' . $tinfo['url'] . '" target="' . $tinfo['target'] . '">' . $tinfo['name'] . '</a></li>';
        }
        $tabhtml .= '<li><a href="http://www.rrzcms.com/Admin/Plugins/plugininfo/code/'.$plugin->info['code'].'.html?flag=help" target="_blank">使用指南</a></li>';
        $tabhtml .= '</ul></div><div class="layui-card-body">';
        $tabhtml .= $this->fetch($temp);
        $tabhtml .= '</div></div>';
        return $tabhtml;
    }
}