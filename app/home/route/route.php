<?php
/**
 * 人人站CMS
 * ============================================================================
 * 版权所有 2015-2030 山东康程信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.rrzcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */

use think\facade\Route;
use \think\facade\Db;


$app = app();

$where = [
    [Db::raw('length(dir_name)'), '>', 0,]
];
$model = M('site_menus')->where($where)->field('id,dir_name');
$app->isDebug() or $model->cache();
$menus = $model->select()->toArray();


$path = C('view.view_path');
$confPath = $path . 'config.json';
$langList = [];
if (is_file($confPath)) {
    $conf = @json_decode(file_get_contents($confPath), true);
    $langList = $conf['lang'] ?? [];
    if (is_string($langList)) {
        $langList = array_filter(explode(',', $langList));
    }
    $langList = array_values(array_unique($langList));
    $langList = array_map(function ($item) {
        if (is_string($item)) {
            $item = ['name' => trim($item),];
        }
        return $item;
    }, $langList);
}

if ($langList) {
    $app->lang->setLangSet($langList[0]['name']);
}


foreach ($menus as $item) {
    Route::rule($item['dir_name'] . '/:page/:pageId', 'Index/menu')->pattern([
        'page' => '\w+', 'pageId' => '\d+',
    ])->complete_match(true)->append(['id' => $item['id'],]);

    Route::rule($item['dir_name'], 'Index/menu')->complete_match(true)->append(['id' => $item['id'],]);

    foreach ($langList as $lang) {
        $lg = $lang['name'] ?? '';
        if (!$lg) continue;

        Route::rule($lg . '/' . $item['dir_name'] . '/:page/:pageId', 'Index/menu')->pattern([
            'page' => '\w+', 'pageId' => '\d+',
        ])->complete_match(true)->append(['id' => $item['id'], 'lang' => $lg,]);

        Route::rule($lg . '/' . $item['dir_name'], 'Index/menu')->complete_match(true)->append(['id' => $item['id'], 'lang' => $lg,]);
    }
}

foreach ($langList as $lang) {
    $lg = $lang['name'] ?? '';
    if (!$lg) continue;

    Route::rule($lg, 'Index/index')->complete_match(true)->append(['lang' => $lg,]);
    Route::rule($lg . '/formSubmit', 'Index/formSubmit')->complete_match(true)->append(['lang' => $lg,]);
    Route::rule($lg . '/search', 'Index/search')->complete_match(true)->append(['lang' => $lg,]);

    Route::rule($lg . '/' . 'menu/:id/:page/:pageId', 'Index/menu')->pattern([
        'id' => '\d+', 'page' => '\w+', 'pageId' => '\d+',
    ])->append(['lang' => $lg,]);

    Route::rule($lg . '/menu/:id', 'Index/menu')->append(['lang' => $lg,]);
    Route::rule($lg . '/article/:id', 'Index/article')->append(['lang' => $lg,]);
    Route::rule($lg . '/node/:id', 'Index/node')->append(['lang' => $lg,]);
    Route::rule($lg . '/cat/:id', 'Index/cat')->append(['lang' => $lg,]);
    Route::rule($lg . '/cats', 'Index/cat')->complete_match(true)->append(['lang' => $lg,]);
    Route::rule($lg . '/brand', 'Index/brand')->complete_match(true)->append(['lang' => $lg,]);
    Route::rule($lg . '/item/:id', 'Index/item')->append(['lang' => $lg,]);

}

Route::rule('formSubmit', 'Index/formSubmit')->complete_match(true);
Route::rule('search', 'Index/search')->complete_match(true);

Route::rule('menu/:id/:page/:pageId', 'Index/menu')->pattern([
    'id' => '\d+', 'page' => '\w+', 'pageId' => '\d+',
]);

Route::rule('menu/:id', 'Index/menu');
Route::rule('article/:id', 'Index/article');
Route::rule('node/:id', 'Index/node');
Route::rule('cat/:id', 'Index/cat');
Route::rule('cats', 'Index/cat')->complete_match(true);
Route::rule('brand', 'Index/brand')->complete_match(true);
Route::rule('item/:id', 'Index/item');